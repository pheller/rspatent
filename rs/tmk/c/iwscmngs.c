/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
/********************************************************************/
/*                                          */
/* IWSCMNGS - common data and initialization termination code     */
/*                                          */
/*    DESCRIPTION - define the global data shared by multitasker */
/*        routines and define initialization routines to set up  */
/*        values at initialization.                        */
/*          The sizes of the tables are controlled by changing   */
/*        the manifest constants below (e.g., to change the RQEs */
/*        allocated), recompiling, and relinking the code. No    */
/*        run time size adjustments are supported.              */
/*          The taking over of the DOS vector (21x) is optional, */
/*        and also controlled by a constant.               */
/*          Initialization claims the following objects:        */
/*         a) DQE 1 = wait manager task. It is the task of    */
/*            last resort for execution during idle time.     */
/*         b) DQE 2 = DOS task = task executing initialization*/
/*            code. Or "main" task.  Whenever we return to DOS*/
/*            to run another program, it is the active task.  */
/*            Priority = 80, on user queue.           */
/*         c) SFVs are of course taken for the above 2 items. */
/*         d) SFV 1 = semaphore for dos serialization. Name   */
/*            = SEM21 and it is usable by appl tasks.      */
/*          We have left the zer entry of each table as         */
/*        unused in order to preserve 0 as a special indicator.  */
/*        This matches the old PLS code. Note then that the table*/
/*        sizes are actually one entry short.              */
/*                                          */
/*    HOW INVOKED - individual calls. Call savevect then inittbls*/
/*                                          */
/*    COMMENTS -                                 */
/*        1. Change the interrupts used if necessary, but beware */
/*        that the application tasks also use to invoke them. */
/*        2. The BIOS timer is handled without an entry in the   */
/*        tables. See timer interrupt handler.             */
/*        3. Resources no longer have character string names.    */
/*                                          */
/********************************************************************/
#include <iwsdefsc.h>
#include <dos.h>

#define RQUES 100
#define DQES 15
#define SFVS 20
#deifne TIMERS 10
#define DOSSER 1

extern int iwsdspg();
extern int iwssvc();
extern int iwstimi();
extern int iwsabend();
extern int iwssusp();
extern int iwsfrih(),iwstoih();
extern int iwswatg();
#if DOSSER
extern int iwsint21();      /* routine that steals DOS int 21s */
#endif

int zsfvmax = SFVS;
int zdqemax = DQES;
int zrqemax = RQES;
int zmaxtimr = TIMERS;
int zsysdspf = 0;    /* dispatcher status flag - idle */

DQE zdqetbl[DQES];   /* the DQEs */
RQE zrqetbl[RQES];   /* the RQEs */
SFV zsfvtbl[SFVS];   /* the SFVs */
unsigned int ztimcntr[TIMERS];            /* timers - current countdown value
unsigned char far *ztimtpop[TIMERS];  /* timer pop flag marked on pop */
int (far *ztimtmad[TIMERS])();           /* second elvel handler address */
unsigned int ztimrdsp[TIMERS];           /* redisp value for timer */
DQE *ztimdqe[TIMERS];          /* requestor's dqe */
unsigned int ztimrest[TIMERS];      /* reset counter   */

DQE *zsyssqh = 0;    /* system queue header */
DQE *zsysuqh = &zdqetbl[2];  /* user queue header   */
DQE *zsyswqh = &zdqetbl[1];  /* wait queue header */
DQE *zsysage = &zdqetbl[2];  /* active dqe ptr      */
DQE *zsys?qh = &zdqetbl[3];  /* extra (free) dqe chain header */
DQE *old_task; /* LH, 4/24/87 -- see iwsdspgs.asm */

RQE *zfrqehd = &zrqetbl[1];  /* free rqe chain header */

/* the sizes of the stacks are unclear as to exactly how much is needed */

unsigned int zstwat[50];     /* wait task stack */
unsigned int zstdsp[50];     /* dispatcher stack */
unsigned int zstint[SIZ_INT_STK]; /* intr hdlr stack */
unsigned char far *zihstkptr =
          (unsigned char far *)&zstint[SIZ_INT_STK];
unsigned char far *zrestkptr;  /* scratch stack pointer save area */
unsigned char far *zdpstkptr = (unsigned char far *)&zstdsp[50];

unsigned char far *callptr;   /* pointer to svc data */
unsigned int dispatch;         /* whether svc results in dispatch */
unsigned char timrdisp = 0;   /* set nonzero if timer tick means dispatch */
int req;             /* request type for this svc */
IWSREGS far *svcregs;          /* pointer to regs saved for svc  */

unsigned int sem21ndx = 1;    /* sfv index of int 21 semaphore   */
unsigned int *sem21own = &zsfvtbl[1].sfvu.sfvd.zsfvmant;  /* owner field */

/* All saved vectors are at time of init and are not necessarily  */
/* pointing directly into DOS or BIOS any more.          */

unsigned long sysvect[8];     /* block of saved interrupt vectors */
unsigned long timrvect;       /* timer interrupt vector */
unsigned long dosvect;         /* dos function call interrupt vector */

unsigned int zcodeseg;         /* our code segment - set in iwsnip */
unsigned int zdataseg;         /* our data segment - set in iwsnip */

static unsigned char hex2asc[] = {"0123456789ABCDEF"};
unsigned char abndtxt[] = {"INTHNDL  ABEND # 0000$"};

void inittbls()
{
  int i;
  DQE *dqeptr;
  RQE *rqeptr;
  IWSREGS *rptr;

  memset(zdqetbl,0,sizeof(zdqetbl));  /* init all dqes to 0 */
  for (i=1,dqeptr=&zdqetbl[1];i<DQES;dqeptr++,i++) {
      dqeptr->zdqedisp = QDISP;   /* dispatchable but suspended */
      dqeptr->zdqeprio = 255;
      dqeptr->zdqeflag = QSUSPND;
      dqeptr->zdqenext = dqeptr+1;
  }
  zdqetbl[DQES-1].zdqenext = 0        /* end of the chain */

  zdqetbl[1].zdqeprio = 250;       /* must be lowest priority used */
  /* Set up a stack for the wait manager task. Start from its high end
     minus the room for one whole save, which we preload for whenever
     the dispatcher runs that task for the first time. Most of the 
     regs are don't cares then.                                        */

  rptr = (IWSREGS *)(((unsigned char *)zstwat) + sizeof(zstwat) -
                sizeof(IWSREGS));
  rptr->sards = zdataseg;       /* extract the data segment value */
  rptr->sarroff = (unsigned int)iwswatg;   /* its code body */
  rptr->sarrseg = zcodeseg;
  rptr->sarpsw = 512;           /* initial flags status when started */
                      /* = interrupts enabled bit on       */

  zdqetbl[1].zdqestak = (unsigned char far *)rptr;
  zdqetbl[1].zdqesfv = 2;          /* take sfv entry */
  zdqetbl[1].zdqenext = &zdqetbl[1]; /* alone on circular chain */

                      /* the active task at start */
  zdqetbl[2].zdqeprio = 0x80;      /* a "user" task  */
  zdqetbl[2].zdqesfv = 3;          /* take sfv entry */
  zdqetbl[2].zdqenext = &zdqetbl[2]; /* alone on circular chain */

  memset(zrqetbl,0,sizeof(zrqetbl));  /* init all rqes to 0 */
  for (i=1,rqeptr=&zrqetbl[1];i<RQES;rqeptr++,i++) {
      rqeptr->zrqenxtp = rqeptr+1;
  }
  zrqetbl[RQES-1].zrqenxtp = 0;       /* end of the chain */

  memset(zsfvtbl,0,sizeof(zsfvtbl));  /* init all sfvs to 0 */
  for (i=1;i<SFVS;i++) {
      zsfvtbl[i].zsfvtag = QSFVTFREE;   /* all sfvs are free */
  }
  zsfvtbl[1].zsfvtag = QSFVTSEM;       /* take sfv for dos sem  */
  zsfvtbl[2].zsfvtag = QSFVTMGR;       /* take sfv for wait task */
  zsfvtbl[2].sfvu.sfvd.zsfvdqea = (unsigned char *)&zdqetbl[1];
  zsfvtbl[3].zsfvtag = QSFVTMGR;       /* take sfv for dos task */
  zsfvtbl[3].sfvu.sfvd.zsfvdqea = (unsigned char *)&zdqetbl[2];

  for (i=0;i<TIMERS;i++) {    /* mark the timers as free */
      ztimtmad[i] = 0;
      ztimcntr[i] = 0;
  }

}

/* save interrupt vectors that we might change */

void savevect()
{
  unsigned long far *p = 0;
  int i;

  dosvect = *(p = 0x21);      /* save dos vector */
  timrvect = *(p + 8);         /* save timer vector */
  p += 0x78;
  for (i=0;i<8;i++) {
      sysvect[i] = *p++;
  }
}

/* restore vectors previously saved */

void restvect()
{
  unsigned long far *p = 0;
  int i;
  *(p + 0x21) = dosvect;      /* save dos vector */
  *(p + 0x8) = timrvect;       /* save timer vector */
  p += 0x78;
  for (i=0;i<8;i++) {
      *p++ = sysvect[i];
  }
}
void setvect()
{
  int far * far *p = 0;

  *(p + 0x7F) = (int far *)iwssusp;  /* set usspend vector */
  *(p + 0x7E) = (int far *)iwsfrih;    /* restore stack int  /*
  *(p + 0x7D) = (int far *)iwstoih;    /* switch stack in    */ 
  *(p + 0x7A) = (int far *)iwssvc;     /* svc handler       */  
  *(p + 0x79) = (int far *)iwsdspg;    /* dispatcher return  */ 
  *(p + 0x78) = (int far *)iwsabend;   /* abend handler      */ 
  *(p + 8)    = (int far *)iwstimi;    /* timer handler      */ 
#if DOSSER
  *(p + 0x21) = (int far*)iwsint21;    /* dos services      */
#endif
}

/* We are abending. Fill in the abend message for shipment to DOS. */

void abndmsg(saverr,saveindx)
unsigned int saverr,saveindx;
{
  int i;
  zsysaqe->zdqeflag = QNOSUSP;           /* make nonsuspendable */
  for (i=4;i;i--) {    /* convert to hex characters */
      abndtxt[16+i] = hex2asc[saverr % 16];
      saverr /= 16;
  }
}
