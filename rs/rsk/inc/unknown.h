/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
/******************** Communications Constants ***********************/
#define reset_cmd_len   4
#define dial_cmd_len    19
#define escape_cmd_len  3
#define hang_cmd_len    4
#define CONNECT          0
#define NOCONNECT        1
#define OK               '0'

/******************** Driver Constants *****************************/
#define SET         1
#define CLEAR       0
#define NOID        0
#define GOODID      1
#define GOODPSW     2
#define BADPSW      3
#define ID_LEN      7
#define PSW_LEN     6
#define MAXIDS      3
#define MAX_ADS     28
#define SOM         0xFF      /* 1rst block of multiblock   */
                              /*  message                   */
#define UNKNOWN     0
#define NO          0
#define YES         1
#define GOOD        0
#define BAD         1
#define IN_SEQ      'I'
#define OUT_SEQ     'O'
#define AGAIN       'A'
#define QUIT        'Q'
/******************** State Indices *******************************/
#define LOGON_ST    0
#define PROFILE_ST  1
#define TRAN_ST     2
#define VD_ST       3
#define VDEND_ST    4
#define LTEX_ST     5
#define LOGOFF_ST   6
#define CPW_ST      7
#define Q_ST        8
#define WELCOME_ST  7
#define NAVIGATE    8
#define KEY_ST      9
/****************************************************************/
/******************************************************************/
#define BLK_SIZE    1024
#define NEW_TX_BLK  998      /* 7/17/87 C.H.       */
#define MULTIBLK    0x02
#define ERROR       0x04
#define RESPONSE    0x20
#define MAXTRYS     1
#define BUFLEN      1024
/******************************************************************/
#define EMPTY       'E'
#define MORE        'M'
#define STAGE       'S'
#define CACHE       'C'
#define MAX         20
/******************************************************************/
#define LOGON_REQ   2
#define LOGOFF_REQ  3
#define ALTUSR_REQ  7
#define MAG_REQ     0x10
#define VD_REQ      0x0e
#define VDEND_REQ   0x0f
#define OBJ_REQ     0
#define CPW_REQ     0x0d
/******************* Secondary Request Codes *****************************
#define LOGON_SCR   1
#define LOGOFF_SCR  0x0f
#define CPW_SCR     0x08
/******************************************************************/
#if NEW_VERSION
#define OBJ_REQ_SZ  15 /* LH, 10/13/87 -- added 2 bytes for version ID */
#else
#define OBJ_REQ_SZ  13
#endif
#define RESP_YES    0x10
#define RESP_NO          0x00           /******* HG *******/
/******************************************************************/
#define ESC         0x1b
#define MACRO       0x40
#define MACRO_NUM   2
#define LEN         19
#define MACRO_OBJ_ID    6
#define AD          35
#define HDR         33
#define VDLOGON_SIZE    85
#define VDCHOICE_SIZE   31
#define VDEND_SIZE  32
#define ERDIS       0
#define BADOBJ      1
#define ERCOM       2
#define ERENTRY     3
#define ERKEY       4
#define ERPSW       5
#define ER3X        6
#define ERLOGON     7
#define ERID        8
#define ERCALL      9
#define ER10        10
#define ERNOTON     11
#define ER12        12
#define ER13        13
#define ER14        14
#define NODICE      15
#define ERCOM1      16



/* ADDITIONS BY HG */

#define STADIR      0       /* stage directory */
#define KEYDIR      1       /* keyword directory */
#define ADKEYDIR    2       /* AD keyword directory */
#define ADLSTDIR    3       /* AD list    directory */


#define ERRWAIT    64000    /* wait period after send_cmd() and before exit */

#define GET              'G'
#define FETCH       'F'

          /* status constants for service manager */

#define END              'E'
#define START_OVER  'S'
#define NOT_FOUND    2
/*   #define NOT_ARRIVED      6         C.H. 3/19/87    */
#define NOT_ARRIVED 21        /*   C. H. 3/19/87    */
#define INVALID_ID  14        /*   C. H. 3/19/87    */
#define OKAY         0
#define FATAL       'F'

#define MAX_PATH    20   /* maximum pathname length (minus filename) */

#define MODEM_WAIT  8    /* delay between modem commands in 10'ths   */
                    /* of seconds                     */
/* #define STD_TO_VAL    30    standard timeout value in secs C.H.7/21/87 */
/* #define  set_to_val( n_secs )   { timval = n_secs * 10; }         C.H.7/21/87 */

#define LEVEL1     1
#define LEVEL2     2
