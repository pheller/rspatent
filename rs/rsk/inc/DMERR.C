/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
/**********************************************************************
*
* FILE NAME:    DMERR.C
*
* DESCRIPTION:
*
*       Contains the module which defines Display Manager error
*       error messages
*
*     DATE            PROGRAMMER          REASON
* -------------------------------------------------------------------
*     20 AUG. '86      M. Silfen          Original
*
/**********************************************************************
*
*                 **** DM ERROR DECLARATIONS *****
*
**********************************************************************/
#include "error.in"

/* Define ERROR types for KM  process. */

#define DE_MALLOC_ERR         0
#define TOO_MANY_WINDOWS 1
#define OUT_OF_MEMORY_ON_OPEN_WINDOW    2
#define WINDOW_NOT_OPEN       3
