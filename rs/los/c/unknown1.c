/* LOS     = 1.0.0    LAST_UPDATE = 1.0.0    */
/*************************************/
/* Author:  L. Wheeler               */
/* Date:    November, 3, 1980        */
/* Purpose: LOS-TMK throwaway code.  */
/* Test a few LOS and TMK calls to   */
/* prove that the multitasker was,   */
/* in fact, installed.  NONSUPPORTED.*/
/*************************************/
#include <iwssvcsc.h>
#include <iwssvccc.h>
#include <rsystem.h>
#include <stdio.h>
int xtask();                           /* declare 2 little TMK tasks */
int ytask();
  static struct {                      /* declare TMK stacks */
      unsigned char stack[500];
      struct env environ;
  } tsk6stk;

  static struct {
      unsigned char stack[500];
      struct env environ;
  } tsk7stk;

  /* declare ICB structs and ptrs */
  ICB xicb[45], yicb, zicb, *xicbp, *yicbp, *zicbp;
  ICB *deq_task(); /* declare LOS dequeue func */

main()
{
  setstk(0); /* TMK requires this to init stack ptr */
  tsk6stk.environ.en_ds = 0; /* required by TMK if task not loaded in */
  tsk7stk.environ.en_ds = 0;

  /* issue TMK request to define tasks (ids of 6 and 7, priority of 80) */

  deftask(7,(int far *)ytask,&tsk7stk.environ,0x80);
  deftask(6,(int far *)xtask,&tsk6stk.environ,0x80);

  /* issue TMK request to dispatch the tasks */

  disptask(5); /* LOS timer task */
  disptask(6); /* "x" task */
  disptask(7); /* "y" task */
  dispret();   /* almighty creator now wishes for "a day of rest" */
               /* i.e. to have TMK suspend him */
}
int xtask()
{
    printf ("xtask: I am alive\n");
    sleep(10); /* LOS function to sleep 10 100 millisecond units */
    for (;;) { /* all tasks are immortal - living "for"ever */
         suspend(); /* LOS function to sleep till work to do */
         yicb.itype = S_REQ; /* when woken up to build an ICB */
         printf("xtask: sending sync request\n");
         enq_task(7, 0, &yicb); /* send a sync request to "y" */
         /* now loop on dequeue until no more ICB's are rcvd */
         while ((xicbp = deq_task()) != (ICB far *)NULL) {
              printf ("xtask: msg # dequeued is %d\n", xicvp -> istatus);
         }
    }
}
int ytask()
{
     int i;

     printf ("ytask: I am alive\n");
     for (;;) {
          printf ("ytask: starting enqueues...\n");
          for (i = 0; i < 3; i++) { /* send a few async requests */
               xicb[i].itype = A_REQ;
               xicb[i].istatus = i; /* putting num in status */
               enq_task(6, 0, &xicb[i]); /* send to "y" */
          }
          zicbp = deq_task(); /* now dequeue an ICB */
          if (zicbp != (ICB far *)NULL) { /* sync or async? */
               printf("ytask:  icb of type %d dequeued\n",
                         zicbp -> itype);
               printf("ytask: sender was %d\n",
                         zicbp -> itask);
               zicb.itype = S_RES; /* send a sync response */
               zicb.istatus = 0xFF;
               enq_task(6, 0, &zicb); /* ... to unblock "x" */
          }
          printf("ytask: preempting...\n");
          preempt(); /* give "x" a chance to run - be generous */
     }
}
