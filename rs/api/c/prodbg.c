/* API     = 1.1.2    LAST_UPDATE = 1.0.0    */
/********************************************************************
*
*                **** STARTUP DEBUGGER ****
*
*********************************************************************
*
* FILE NAME                PRODBG.C
*
* DESCRIPTION:
*
*
*     DATE            PROGRAMMER          REASON
* -------------------------------------------------------------------
*   11/05/85            R.G.R.           ORIGINAL
*
*********************************************************************/
#include <conio.h>
#include <SMDEF.IN>
#include <OBJUNIT.IN>
#include <SMPPT.IN>
#include <SMKEYS.IN>
#include <stdio.h>
#include <stdlib.h>
#include <INSERR.IN>
#include <RTADEF.IN>
#include <RTASTR.IN>
#include <DBGMACRO.IN>
#include <PACKSTR.IN>
#include <apityp.in>
#include <SYMBOL.IN>
#include <ctype.h>
#include <ISCB.IN>
#include <DBGSTACK.IN>
/*********************************************************************
*
*                  **** DEFINITIONS ****
*
*********************************************************************/
#define FILESIZE (8 * 1024)
#define GO  0
#define NOGO 1
#define MSB  1
#define LSB  0
#define CONTINUE  0xFFFF
#define RTN  1
#define NORTN 0

/*********************************************************************
*
*                    **** EXTERNALS ****
*
*********************************************************************/
extern unsigned char FAR *ip;
extern unsigned char FAR *strtloc;
extern unsigned char indata[];

extern struct rta rtatbl[];
extern struct glbvlist FAR *(*glbmid);
extern struct linklist FAR *(*savelist);

extern WINDOW FAR *W;

extern unsigned char reset;
extern unsigned char goflg;
extern unsigned char FAR *strtaddr;

extern unsigned char membrknum;
extern unsigned char brknum;
extern Objstream stream_ptr;

extern ISCB *level0;

extern TBOLPROG tbol_str;
extern unsigned char data;
extern unsigned char apirtn;

extern struct EVENT event;

extern unsigned int Sys_api_event;
extern unsigned char alt_display_flag;

/*********************************/
extern unsigned char indata[];
extern unsigned char bcount;
extern unsigned char psmode;
extern struct symbol symtbl[];
extern struct symbol *symrec;
extern struct symbol *symend;

/*********************************************************************
*
*                 **** VARIABLE DECLARATIONS ****
*
*********************************************************************/
typedef union lonptr
{
unsigned char *ptr;
unsigned int iary[2];
}LPTR;

typedef union intary
{
   unsigned char cary[2];
   unsigned int ivar;
}IARY;

typedef struct prog_type
{
unsigned char typ_ary[23];
}PTYP;

PTYP prog_typ_array[] =
{
{"SELECTOR              \0"},
{"FIELD INITIALIZER     \0"},
{"ELEMENT INITIALIZER   \0"},
{"PAGE INITIALIZER      \0"},
{"FIELD POST PROCESSOR  \0"},
{"ELEMENT POST PROCESSOR\0"},
{"PAGE POST PROCESSOR   \0"},
{"ELEMENT HELP          \0"},
{"PAGE HELP             \0"},
{"FILTERED FUNCTION     \0"},
{"OVER_RIDE FILTER FUNC.\0"},
{"RE-ENTER              \0"},
{"TRINTEX_ASSISTANT     \0"},
};

unsigned char FAR *dbg_ip;

FILE *fileptr;
unsigned int numread;

FILE *symptr;
unsigned int symread;
unsigned char symname[16];

unsigned char debug_init;
unsigned char debug_re_entry;

DWS dbg_w_stack[DBG_STACK_SIZE+1];     /* Allow for 16 windows and 30 calls. */
DWS *dbg_w_ptr;
DWS *dbg_w_end;

int tbol_prog_offset;

unsigned char mem_flag;
/********************************************************************
*
*                **** SAVE DEBUGGER ENVIRONMENT ****
*
*********************************************************************
*
* ROUTINE NAME:         save_debug_env ()
*
* DESCRIPTION:
*
*     DATE            PROGRAMMER          REASON
* -------------------------------------------------------------------
*   11/05/85            R.G.R.           ORIGINAL
*
*********************************************************************/
save_debug_env()
{

/* Adjust debug env. save stack pointer to next empty entry. */
--dbg_w_ptr;

/* Save debug environment. */
dbg_w_ptr->stack_ip = ip;

if ((dbg_w_ptr->stack_stream_offset = Obj_get_offset(stream_ptr)) < 0)
   return(NO_PROCESS);

return(NO_ERROR);

}
/********************************************************************
*
*                **** RESTORE DEBUGGER ENVIRONMENT ****
*
*********************************************************************
*
* ROUTINE NAME:         restore_debug_env ()
*
* DESCRIPTION:
*
*     DATE            PROGRAMMER          REASON
* -------------------------------------------------------------------
*   11/05/85            R.G.R.           ORIGINAL
*
*********************************************************************/
restore_debug_env()
{

if (dbg_w_ptr == dbg_w_end)
    return(NO_PROCESS);

/* Restore debug environment. */
strtloc = strtaddr = ip = dbg_w_ptr->stack_ip;

/* Restore current stream pointer. */
if (Obj_seek(stream_ptr, dbg_w_ptr->stack_stream_offset, 0) != 0)
    return(NO_PROCESS);

/* Adjust debug env. save stack pointer to next entry. */
++dbg_w_ptr;

return(NO_ERROR);
}
/********************************************************************
*
*                **** INITIALIZE DEBUGGER ****
*
*********************************************************************
*
* ROUTINE NAME:         debug_startup ()
*
* DESCRIPTION:
*
*     DATE            PROGRAMMER          REASON
* -------------------------------------------------------------------
*   11/05/85            R.G.R.           ORIGINAL
*
*********************************************************************/
debug_startup (prog,run_flag)

TBOLPROG FAR *prog;
unsigned int run_flag;

{

unsigned int  index;
union intary prg_length;
union intary filename_length;
unsigned char symname[80];
union intary main_offset;
union intary start_offset;
unsigned int count;
unsigned int count1;
unsigned int cnt;
unsigned int main_loc;
RTA *rt;
PTYP *typtr;
char tbuf[20], d_t[16], ver[6];
int verflag, i, j;

/* Return if debugger has been initialized. */
if(debug_init)
   return(NO_ERROR);

if (run_flag == START)
{
    if (prog->prog_obj.status == OBJREFOBJUNDEFINED)
       return(STOP);

    /* Switch screen to text mode. */
    wait_icon();
    if(!alt_display_flag){
        event.event = DEBUG_MODE;
        do_display_event (event);
    }

    /* If an error occurre during a fetch then return with error. */
    if(prog->prog_obj.status == OBJREFERROR)
    {
         printf ("OBJECT REFERENCE ERROR\n");
         wait_for_key();
         return(PROG_ERROR);
    }

    if((prog->prog_obj.status == OBJREFNOTACCESSED)
                             ||
       (prog-<prog_obj.status == OBJREFFETCHED))
    {
       if ((stream_ptr = Obj_ref_get(&prog->prog_obj)) == NULL)
       {
                printf("TRINTEX OBJECT GET ERROR\n");
                wait_for_key();
                return(NO_PROCESS);
            }
        } else {
            /* Object has been accssed, therefore set up stream pointer */
            stream_ptr = prog->prog_obj.varptr.streamptr;

            if (Obj_seek(stream_ptr, 0, 0) != 0) {
                printf("RESET STREAM POINTER FAILURE\n");
                wait_for_key();
                return (NO_PROCESS);
            }
        }
    } else {
        /* Sswitch screen to text mode. */
        wait_icon();
        if(!alt_display_flag) {
            event.event = DEBUG_MODE;
            do_display_event(event);
        }

        /* Rest program stream pointer,. */
        if (Obj_seek(stream_ptr, 0, 0) != 0) {
            printf("RESET STREAM POINTER FAILURE\n");
            wait_for_key();
            return (NO_PROCESS);
        }
    }

    /* Adjust stream pointer to point to start of program. */
    if ((Obj_seek(stream_ptr, 22, 0)) != 0) {
        printf("ADJUST STREAM POINTER FAILURE\n");
        wait_for_key();
        return(NO_PROCESS);
    }

    /* Get length(MSB) of tbol program... */

}
