/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
/********************************************************************/
/*                                          */
/* IWSSVCGS - main handler of SVCs                    */
/*                                          */
/*    DESCRIPTION - the part of SVC handling that could be coded */
/*        in C resides in this module. It is invoked by the     */
/*        interrupt handler and returns to it. The interrupt     */
/*        handler is responsible for (before calling here):     */
/*         a) saving registers on the proper stack         */
/*         b) setting svcregs to the address of the top of    */
/*            the saved set so we can get at the original     */
/*            values                             */
/*         c) setting ds register to our local segment     */
/*         d) setting callptr to the es:dx pair that is needed*/
/*            on most SVCs as the address of the parms.         */
/*         e) setting variable "req" to the request type      */
/*        On return, we may have modified the saved registers    */
/*        for when they go back to the caller. We also set the   */
/*        dispatch flag to tell the interrupt handler to go to   */
/*        the dispatcher for a possible task switch.       */
/*                                          */
/*    HOW INVOKED - called by iwsinth with no arguments. (Req    */
/*        type (was bx) is in variable "req").                */
/*                                          */
/*    Comments -                                 */
/*        1. Some of the SVCs were split out only because the    */
/*        compiler struggled with a module of that size.        */
/*        2. The code is not reentrant. Maybe some of it could   */
/*        be but remember that much of it involves manipula-  */
/*        tion of queues that must be protected anyway.         */
/*        3. Most errors result in fatal abends without signal   */
/*        back to the requesting task.                */
/*        4. The shutdown svc is invoked to get the multitasker  */
/*        origin and restore the original state in order to   */
/*        remove it from memory.                      */
/*        5. Some SVCs that set a task dispatchable now will     */
/*        case the dispatcher to run only if the state change*/
/*        activates a higher priority task.          */
/*                                         */
/********************************************************************/
#include <iwsdefsc.h>
#include <iwssvccc.h>
#include <iwssvcsc.h>
#include <dos.h>

/* external variables */

extern IWSREGS far *svcregs;
extern unsigned char far *callptr;   /* pointer to SVC parameter list */
extern SFV zsfvtbl[];
extern DQE zdqetbl[];
extern RQE zrqetbl[];

extern unsigned int ztimcntr[];      /* timer tables */
extern unsigned int ztimrest[];
extern unsigned int ztimrdsp[];
extern unsigned char far *ztimtpop[];
extern int (far *ztimtmad[])();
extern DQE *ztimdqe[];

extern int zsfvmax;
extern int zdqemax;
extern int zrqemax;
extern DQE *zsysaqe;      /* active task dqe */
extern RQE *zfrqehd;      /* free rqe chain  */
extern int dispatch;      /* nonzero if dispatch required */
extern int zmaxtimr;
extern DQE *zsyssqh,*zsysuqh,*zsyswqh,*zsyseqh;
extern int req;       /* the SVC wanted */
extern int _psp;      /* holds segment address of program's origin */

/* external routines */

extern void restvect();
extern void abend();
extern void svcmgrmk();
extern void svcremove();
extern iwstims();

/* local variables */

RQE *svcrqeptr;  /* pointer to RQE entry */
DQE *svcdqeptr;  /* pointer to DQE entry */
SFV *svcsfvptr;  /* pointer to requested SFV entry */
int callsfv;    /* the SFV relevant to the SVC    */
static unsigned char far *parlist;  /* scratch pointer to parm data */
static RQE *rqp1,*rqp2;      /* scratch RQE pointers */
static int i;                /* misc loop counter    */

void iwssvcg()
{

     dispatch = 0;        /* default = no dispatch */

     /* map the ax value which is usually an SFV index into the  */
     /* corresponding SFV location. Set svcsfvptr.           */
     /* An sfv value out of range is set to zero and handled by the */
     /* individual SVC.                           */

     callsfv = svcregs->sarax;   /* get sfv index */
     if ((callsfv < 0) || (callsfv >= zsfvmax)) {
          callsfv =            /* out of range  */
          svcregs->sarax = 0;
     }
     svcsfvptr = &zsfvtbl[callsfv];    /* point to the entry */

     switch (req) {
          /************************************************************/
          /*                                     */
          /* Send attention request from a task or an interrupt      */  
          /* handler. No reply is returned. The parm list (es:dx)     */
          /* points to the 14-bytes of request data that are copied   */
          /* into an extra RQE. One RQE shows up on the request queue */
          /* of the recipient and it points to the other RQE with the */
          /* data.                               */
          /*   On an interrupt attention, the invoking handler must   */
          /* have saved the interrupted task's registers because      */
          /* we return to the dispatcher or the original task, not    */
          /* the interrupt handler. (See this routine's caller.)      */
          /*                                      */
          /************************************************************/

     case _QSVCATN:
     case _QSVCIATN:
          if (!callsfv)
               svcabend(SFVIATN);
          if (svcsfvptr->zsfvtag != QSFVTMGR)
               svcabend(WRMANOP);
          getrqe();
          parlist = (unsigned char far *)svcrqeptr;
          *(long *)(svcrqeptr) = *(long far *)(callptr);
          *((long *)(svcrqeptr) + 1) = *((long far *)(callptr) + 1);
          *((long *)(svcrqeptr) + 2) = *((long far *)(callptr) + 2);
          *((int *)(svcrqeptr) + 6) = *((int far *)(callptr) + 6);
          mancall();
          break;

          /************************************************************/
          /*                                     */
          /* Claim the semaphore identified by ax and go into a     */
          /* semaphore wait state if the semaphore is already claimed.*/
          /* The zsfvmant field of the semaphore holds the id of who  */
          /* has the semaphore. The semaphore SFV entry has a header  */
          /* pointer to a list of RQEs that describe who is waiting   */
          /* for the semaphore. Any additions always go at the end    */
          /* (FIFO) without regard to the waiting task's priorities.  */
          /*                                     */
          /************************************************************/

     case _QSVCCLAIM:
          if (!callsfv)
               svcabend(SFVCLAIM);
          if (svcsfvptr->zsfvtag != QSFVTSEM)
               svcabend(SEMCLAIM);
          if (svcsfvptr->sfvu.sfvd.zsfvmant == 0) {   /* unclaimed */
               svcsfvptr->sfvu.sfvd.zsfvmant = zsysaqe->zdqesfv;
          }
          else {
               parlist = 0;              /* no parameters to save   */
               zsysaqe->zdqedisp = QSEMWAIT;  /* put requestor into wait */
               semrqe();            /* ..for semaphore state   */
               dispatch = 1;             /* find another task to run*/
          }
          break;

          /************************************************************/
          /*                                     */
          /* Release the semaphore identified by ax and pass it on    */
          /* to the first task in the queue, if any, and wake up that */
          /* task. There need not be a match between claimer and     */
          /* sender, whcih is good for wait/post simulation as     */
          /* documented elsewhere, but also bad in that it lets     */
          /* another task steal the semaphore.                  */
          /*                                     */
          /* If a task was waiting, then the zsfvdqea field is the    */
          /* pointer to the first rqe that says who is waiting.     */
          /* The RQE zrqenxtp field says who is next in line, and the */
          /* zrqedqep fields points to that task's DQE.               */
          /*                                     */
          /************************************************************/

     case _QSVCREL:
          if (!callsfv)
               svcabend(SFVREL);
          if (svcsfvptr->zsfvtag != QSFVTSEM)
               svcabend(SEMREL);
          svcsfvptr->sfvu.sfvd.zsfvmant = 0;   /* release sem */

          /* if someone waiting, mark dispatchable 1st guy on queue */

          if (svcsfvptr->sfvu.sfvd.zsfvdqea) {   /* if someone waiting */
               svcrqeptr = (RQE *)svcsfvptr->sfvu.sfvd.zsfvdqea;
               svcsfvptr->sfvu.sfvd.zsfvdqea = (unsigned char *)svcrqeptr->zr
               /* we have removed it from queue */
               svcdqeptr = (DQE *)svcrqeptr->zrqedqep;
               svcsfvptr->sfvu.sfvd.zsfvmant = svcdqeptr->zdqesfv;  /* claime
               if (svcdqeptr->zdqedisp != QSEMWAIT) {
                    svcabend(NOTSEMWAIT);    /* wasnt waiting for semaphore!
               }
               putrqe();                 /* release the rqe */
               svcdqeptr->zdqedisp = QDISP;  /* target now dispatchable */
               if (svcdqeptr->zdqeprio < zsysaqe->zdqeprio)  /* if high prio
                    dispatch = 1;
          }
          break;

          /************************************************************/
          /*                                     */
          /* The application wants to shutdown and remove pgm. Assume */
          /* all tasks are idle and preferably deleted. We restore the*/
          /* interrupt vectors and return our PSP origin address so   */
          /* the application can release our storage.           */
          /*                                     */
          /************************************************************/

     case _QSVCSHUT:
          restvect();         /* restore vectors */
          svcregs->sarax = _psp;   /* resident origin address */
          break;

          /************************************************************/
          /*                                     */
          /* Return the id of the task running. Pass back in ax.     */
          /*                                     */
          /************************************************************/

     case _QSVCCURSFV:
          svcregs->sarax = zsysaqe->zdqesfv;     /* my id */
          break;

          /************************************************************/
          /*                                     */
          /* Create semaphore.                                        */
          /* The id cannot match any existing semaphore or task.      */
          /*                                     */
          /************************************************************/

     case _QSVCSEMMK:
          svcsfvptr -> zsfvtag = QSFVTSEM;
          svcsfvptr -> sfvu.sfvd.zsfvdqea = 0;
          svcsfvptr -> sfvu.sfvd.zsfvmant = 0;
          break;
   
          /************************************************************/
          /*                                     */
          /* Define a task. The parameter list (es:dx) points to a    */
          /* structure with the form: bytes 0-7 = name of task,     */
          /* bytes 8-9 = 0 and hold the assigned SFV index on return, */
          /* bytes 10-13 = long pointer (offset first) to the stack   */
          /* address for the task, byte 14 = task priority, byte 15 = */
          /* suspend/nonsuspend state of new task.              */
          /*                                     */
          /* The stack for the task must have been set up before     */
          /* invoking the SVC, with the 24 bytes with the registers   */
          /* and entry point and flags already saved and pointed at   */
          /* by the stack pointer in the request. This is the save    */
          /* format needed by the dispatcher for when this task is    */
          /* eventually started for the first time. At least the seg- */
          /* ment regs must be filled in for those saved values as    */
          /* this SVC won't do it.                                    */
          /*                                     */
          /************************************************************/

     case _QSVCMGRMK:
          svcmgrmk();
          break;

          /************************************************************/
          /*                                     */
          /* Set dispatchable the task specified by the SFV index in  */
          /* ax, if an only if the task is in the non-dispatchable    */
          /* state.  On return, ax = 0 if the required state    */
          /* change was made and holds the dispatch status of the     */
          /* task if it was some other state.                   */
          /*   If a state change occurs and if CX is nonzero on entry,*/
          /* then a dispatch will be performed.                 */
          /*   One reason not to dispatch is if this SVC comes from   */
          /* an interrupt handler that must finish before going to    */
          /* the dispatcher.                          */
          */                                     */
          /************************************************************/

     case _QSVCDSPT:
          if (!callsfv)
               svcabend(SFVDSPT);
          if (svcsfvptr->zsfvtag != QSFVTMGR)
               svcabend(MANDSPT);
          if ((svcregs->sarax =
              (svcdqeptr = ((DQE *)(svcsfvptr->sfvu.sfvd.zsfvdqea)))->zdqedis
              QNODISP) {
               svcdqeptr->zdqedisp = QDISP;  /* make dispatchable */
               dispatch = svcregs->sarcx;    /* maybe go to dispatcher */
          }
          break;

          /************************************************************/
          /*                                     */
          /* Get first item from request queue of the active task.    */
          /* exist. If the queue is empty, cx, dx, and es are set to  */
          /* zero.                               */
          /*                                     */
          /* Assume the entry does exist. On return ax = sfv index    */
          /* (taskid) of the sender, si = pointer to the dqe of the   */
          /* sender (relative to this segment) es:dx = long address   */
          /* of the parameter list, and cx = a tag value to identify  */
          /* the rqe at later completion. Cx is in fact the rqe addr. */
          /*                                     */
          /* Registers other than cx on input are not used.     */
          /*                                     */
          /************************************************************/

     case _QSVCGETRQ:
          rqp1 = zsysaqe->zrqhfrt;      /* default = first item */
          if (rqp1 == 0) {         /* no such request */
               svcregs->sarcx = svcregs->sares = svcregs->sardx = 0;
          }
          else {
               svcregs->sarcx = (unsigned int)rqp1;  /* tag = rqe address */
               
               /* return ax = sfv value, si = address of sender's dqe */

               svcregs->sarax =
                   ((svcregs->sarsi = (unsigned int)(rqp1->zrqedqep)) == 0) ?
               0 : ((DQE *)rqp1->zrqedqep)->zdqesfv;
               svcregs->sardx = *((int *)(&rqp1->zrqeplst));
               svcregs->sares = *(((int *)(&rqp->zrqeplst)) + 1);

          }
          break;

          /************************************************************/
          /*                                     */
          /* Complete request. The item on the request queue is     */
          /* identified by cx (actually equal to rqe address). cx = 0 */
          /* to complete the first item. The value in cx is the tag   */
          /* value returned by the get request service. The function  */
          /* is a no-op the queue is empty. The RQE (and the     */
          /* parameter RQE on an attention) are returned to the     */
          /* free queue on the attention or full wait forms, and to   */
          /* the completion queue of the sender in other types.     */
          /*                                     */
          /************************************************************/

     case _QSVCCMPRQ:
          if ((svcrqeptr => zsysaqe->zrqhfrt) != 0) {    /* queue empty? */
               rqp2 = 0;           /* no predecessor yet  */
               if (svcregs->sarcx) {         /* want a specific one? */
                    while (svcrqeptr && (svcrqeptr != (RQE *)svcregs->sarcx))
                         rqp2 = svcrqeptr;
                         svcrqeptr = svcrqeptr->zrqenxtp;
                    }
                    if (!svcrqeptr)
                         svcabend(BADCMPRQ);
               }

               /* svcrqeptr = pointer to the thing to complete */
               /* rqp2 = pointer to the one ahead of it on the chain */

               if (rqp2 == 0) {
                    zsysaqe->zrqhfrt = svcrqeptr->zrqenxtp;   /* new front */
               }
               else {
                    rqp2->zrqenxtp = svcrqeptr->zrqenxtp;  /* move link aroun
               }
               if (zsysaqe->zrqhlst == svcrqeptr)
                    zsysaqe->zrqhlst = rqp2;         /* new back end */
               if ((svcrqeptr->zrqerqst == _QSVCATN) ||
                   (svcrqeptr->zrqerqst == _QSVCIATN)) {
                    /* discard parm list copy rqe if an attention */
                    rqp1 = svcrqeptr;    /* temp save */
                    svcrqeptr = *(RQE **)(&svcrqeptr->zrqeplst);
                    putrqe();       /* free the rqe */
                    svcrqeptr = rqp1;
                    putrqe();   /* release rqe that was on the queue */
               }
          }
          break;

          /************************************************************/
          /*                                     */
          /* Set the timer to the specified nuber of ticks. Adjust    */
          /* from the old scale to the actual clock rate.      */
          /*    ax = timer index. Value specified in cx.       */
          /*                                     */
          /************************************************************/

     case _QSVCSETTIM:
          if ((callsfv < 1) || (callsfv > zmaxtimr))
               svcabend(ILLEGTIM);
          ztimcntr[callsfv] = (svcregs->sarcx) ?
          ((svcregs->sarcx - 1) / QTIMADJ  + 1) :
          0;        /* shut to zero */
          break;

          /************************************************************/
          /*                                     */
          /* Create a timer. Assign timer to current task and set     */
          /* its pointer address for a pop variable.            */
          /*    ax = timer index. Value returned in cx.          */
          /*    callptr points to the original plist. Contents:     */
          /*   arg 0 = time in ticks to reset to after pop      */
          /*   arg 1/2 = address of variable to set when pop     */
          /*                                     */
          /************************************************************/

     case _QSVCTIMMK:
          if ((callsfv < 1) || (callsfv > zmaxtimr))
               svcabend(ILLEGTIM);
          ztimtmad[callsfv] = iwstims;    /* define processing routine */
          ztimcntr[callsfv] = 0;         /* don't run the timer yet   */
          ztimrest[callsfv] =       /* reset count */
          (((struct svcplst far *)callptr)->sp_args[0] == 0) ? 0 :
          ((((struct svcplst far *)callptr)->sp_args[0] - 1) / 16 + 1);
          ztimrdsp[callsfv] = QDISP;      /* dispatch status */
          ztimdqe[callsfv] = zsysaqe;     /* owning task     */
          ztimtpop[callsfv] = *(unsigned char far * far *)
               &(((struct svcplst far *)callptr)->sp_args[1]);
          break;

          /************************************************************/
          /*                                     */
          /* Unrecognized SVC code. Issue an abend.             */
          /*                                     */
          /************************************************************/

     default:
          svcabend(ILLEGSVC);   /* never come back */
     }  /* end switch */
     /* fall back to caller to restore regs, etc. */
}

/********************************************************************/
/* svcabend - shared by the SVC routines that issue the abend. It   */
/*     simply passes the abend signal through to the more common    */
/*     abend routine with the QSVCINDX included as the source id.   */
/*     Do not expect a return.                             */
/*                                          */
/********************************************************************/
svcabend(errcode)
unsigned int errcode;
{
     abend(QSVCINDX,errcode);
}

/********************************************************************/
/* getrqe - take the first RQE from the free queue and return its   */
/*     address in svcrqeptr. Abend if the list is empty.        */
/*                                          */
/********************************************************************/
static getrqe()
{
     if ((svcrqeptr = zfrqehd)) {
          zfrqehd = zfrqehd->zrqenxtp;
     }
     else {
          svcabend(SVCNORQE);
     }
}

/********************************************************************/
/* putrqe - return an RQE to the free queue. Add it to the front.   */
/*    Svcrqeptr is the address of the RQE.                */
/*                                          */
/********************************************************************/
static putrqe()
{
     svcrqeptr->zrqenxtp = zfrqehd;    /* add to front of list */
     zfrqehd = svcrqeptr;
}

/********************************************************************/
/* semrqe - sets up and enqueues an RQE for claim semaphore ops.    */
/*     Variable scvrqeptr is set to the address of the RQE to use.  */
/*     The RQE is added to the end (not front) of the chain for the */
/*     semaphore identified by svcsfvptr.                  */
/*     Putting the task in a wait state must be done elsewhere.  */
/*     If the semaphore had been free, mark in its sfv that this */
/*     task now has it. (It's done indirectly by pointing to the RQE*/
/*     that has the task pointer in it.)                   */
/*                                          */
/********************************************************************/
static semrqe()
{
     RQE *svcp1;

     getrqe();      /* obtain a free rqe */
     svcrqeptr->zrqenxtp = 0;     /* add to the end */
     svcrqeptr->zrqedqep = (unsigned char *)zsysaqe;  /* remember which task
     svcrqeptr->zrqeplst = parlist;  /* original request parms address */

     /* if list is empty, set the header, else add to list */

     if ((svcp1 = (RQE *)svcsfvptr->sfvu.sfvd.zsfvdqea) == 0) {
          svcsfvptr->sfvu.sfvd.zsfvdqea = (unsigned char *)svcrqeptr;
     }
     else {      /* find end of chain and add it in */
          while (svcp1->zrqenxtp) {
               svcp1 = svcp1->zrqenxtp;
          }
          svcp1->zrqenxtp = svcrqeptr;
     }
}

/********************************************************************/
/* mancall - enqueues the request on the target task's queue.       */
/*     Get an RQE to hold the request data and place the RQE in the */
/*     queue.                                                       */
/*     If the target task was not dispatchable, set it dispatch- */
/*     able in order to be awakened to process. (Only if "NODISP"   */
/*     because we don't wake it up if in a semaphore wait, for      */
/*     example.)                                 */
/*                                          */
/********************************************************************/
mancall()
{
     svcdqeptr = (DQE *)svcsfvptr->sfvu.sfvd.zsfvdqea;   /* receiver task */
     getrqe();
     svcrqeptr->zrqedqep = (req==_QSVCIATN) ? 0 : (unsigned char *)zsysaqe;
     svcrqeptr->zrqeplst = parlist;         /* save request data */
     svcrqeptr->zrqerqst = req;

     /* Place request on queue. Mask off high priority bit. */
     /* A priority of 0 always goes at the very end.    */

     if ((svcrqeptr->zrqeprty = svcregs=>sarcx & 0x7fff) == 0) {
          rqp1 = 0;
          rqp2 = svcdqeptr -> zrqhlst; /* add to end of list */
     }
     else { /* put in priority order */
          rqp2 = 0;
          rqp1 = svcdqeptr -> zrqhfrt;
          while ((rqp1 != 0) && (rqp1 -> zrqeprty >= svcrqeptr -> zrqeprty))
               rqp2 = rqp1;
               rqp1 = rqp1 -> zrqenxtp;
          }
     }
     svcrqeptr -> zrqenxtp = rqp1;
     if (rqp2)
          rqp2->zrqenxtp = svcrqeptr;
     else
          svcdqeptr->zrqhfrt = svcrqeptr;     /* this is the new front */
     if (!rqp1)
          svcdqeptr->zrqhlst = svcrqeptr;     /* this is the new end   */

     /* the code to avoid extraneous dispatches is not ideal because of
               a particular case. A task can be set dispatchable without forc
               a dispatcher cycle. We can end up with a situation where the h
               priority dispatchable task is not running. Since this path con
               RQEs, we are better off forcing extra dispatches to make sure
               don't send to a task left in that state.

     if (svcdqeptr->zdqedisp == QNODISP)
          svcdqeptr->zdqedisp = QDISP;   /* task may now wake up   */
     if (svcdqeptr->zdqeprio < zsysaqe->zdqeprio)   /* if high prio */
          dispatch = 1;
}

