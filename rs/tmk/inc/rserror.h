/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
/********************************************************************/
/*                                          */
/* RSERROR  - "C" error codes for Reception System Logical O/S      */
/*                                          */
/*    HOW INVOKED - via "#include <rserror.h>"                */
/********************************************************************/
/* define logical operating system error codes */

#define   E_NONE               0
#define E_BADICB         -1
#define E_TSKID               -2
#define E_NO_TIMER       -3
#define E_TIME           -4
#define E_TIMR_NOT_CREATED    -5
#define E_TIMRID         -6
#define E_PRIORITY       -7
#define E_MSG_TYPE       -8
/********************************************************************/
/*   END RSERROR                                 */
/********************************************************************/
