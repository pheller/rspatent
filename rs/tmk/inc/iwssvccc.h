/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
/********************************************************************/
/*                                          */
/* IWSSVCCC - "C" equates for multitasker constants for appls       */
/*                                          */
/*    DESCRIPTION - equates/defines to preface a C program that  */
/*        needs certain multitasker equates, as for SVCs        */
/*                                          */
/*    HOW INVOKED - via "#include <iwssvccc.h>"                  */
/*                                          */
/********************************************************************/
#define _QABEND   0x0078      /*IWS abend interrupt vector     */
#define _QDISPRET 0x0079      /*dispatcher return interrupt vector */
#define _QSVC    0x007a      /*SVC handler request             */
#define _QTOIHSTK 0x007d      /*SWAPTOIHentry point address    */
#define _QFRIHSTK 0x007e      /*SWAPBAK entry point address    */
#define _QSUSPEND 0x007f      /*SUSPEND entry point address    */
/******************************************************************/
/* Dispatcher related constants for task services.            */
/******************************************************************/
#define  _QNODISP      0      /* task non dispatchable    */
#define  _QDISP        1      /* dispatchable             */
#define  _QWAIT        2      /* task waiting             */
#define  _QUSET        3      /* dispatcher decides       */
#define  _QUSETWT      4      /* dispatcher dec(with timer) */
#define  _QSUSPND      0      /* task suspendable    */
/******************************************************************/
/* SVC request type constants                            */
/******************************************************************/
#define _QSVCATN    2    /*  attention       */
#define _QSVCIATN   3    /*  IH attention          */
#define _QSVCCMPRQ  4    /*  complete request      */
#define _QSVCGETRQ  7    /*  get request           */
#define _QSVCCLAIM  8    /*  claim semaphore       */
#define _QSVCREL    9    /*  release semaphore     */
#define _QSVCDSPT   10   /*  set dispatchable      */
#define _QSVCSETTIM 11   /*  set timer count       */
#define _QSVCTIMMK  12   /*  create a timer  */
#define _QSVCSHUT   14   /* shutdown         */
#define _QSVCCURSFV 15   /*  get current SFV index  */
#define _QSVCSEMMK  17   /*  create semaphore      */
#define _QSVCMGRMK  18   /*  create manager  */
/******************************************************************/
/*   END IWSSVCCC                               */
/******************************************************************/
