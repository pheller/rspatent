/* API     = 1.1.2    LAST_UPDATE = 1.0.0    */
/**********************************************************************
*
*    **** API INTERPRETER INSTRUCTION OPERAND RESOLVE TABLE ****
*
************************************************************************
*
* DESCRIPTION:
*         This file contains the API instruction operand table,
* which is used by the interpreter to resolve the operands of each
* instruction. The instruction state is used to determine if the
* process operand module should be called to resolve the instruction
* operands (state = 0 means call procoper () ). The next field is the
* number of operands to process followed by the enable of external
* variable searches for partition and global variables.
*
*   DATE  PROGRAMMER           REASON
* --------------------------------------------------------------------
* 04/30/86     R.G.R.               ORIGINAL
*
***********************************************************************/

struct procop
{
    unsigned char instr_state;     /* Whether to resolve procoper in TBOLDRV
    unsigned char nbr_of_oper;     /* Number of operands */
    unsigned char mod_args;   /* These args will be modified by the verbs */
#if 0
    unsigned char part_mask;  /* old mask to search for PEV's */
    unsigned char glob_mask;  /* old mask to search for GEV's */
#endif
};

#define P0  0
#define P1  PAR1OP
#define P2  PAR2OP
#define P3  PAR3OP
#define P4  PAR4OP
#define P5  PAR5OP
#define P6  PAR6OP
#define P7  PAR7OP
#define P12 (PAR1OP | PAR2OP)
#define P13 (PAR1OP | PAR3OP)
#define P23 (PAR2OP | PAR3OP)

/*
    I cannot discern the reason for using the part_mask and glob_mask to
    verify whether certain operands are PEV's or GEV's, because it can only
    be useful for detecting compiler errors, and every instruction allows
    every arg to be a PEV or GEV anyway, with the following notable
    exceptions:
 
     2nd arg of FILL (pattern)
     1st arg of LINK (program name)
     All args of MOVE BLOCK
 
    Obviously MOVE BLOCK's args can't be PEV's or GEV's, but the compiler
    shouldn't generate them.  And I can see no reason why the rest shouldn't
    be PEV's or GEV's.  These masks were therefore removed.
 */

struct procop ins_oper_tbl[] = {
  1, 0, 0,    /* P0OP, P0OP,   BRK */           
  0, 2, 0,    /* P2OP, G2OP,   CJEQ */          
  0, 2, 0,    /* P2OP, G2OP,   CGNE */          
  0, 2, 0,    /* P2OP, G2OP,   CGLT */          
  0, 2, 0,    /* P2OP, G2OP,   CJGT */          
  0, 2, 0,    /* P2OP, G2OP,   CJLE */          
  0, 2, 0,    /* P2OP, G2OP,   CGJE */          
  0, 0, 0,    /* P0OP, P0OP,   JMP */           
  0, 5, 0,    /* P5OP, G5OP,   DEF_FLD */       
  0, 6, 0,    /* P6OP, G6OP,   DEF_FLD_PRG */   
  0, 1, 0,    /* P1OP, P0OP,   SET_ATT */       
  0, 2, P0,   /* P2OP, G2OP,   OPEN */          
  0, 0, P0,   /* P0OP, P0OP,   CLOSE_ALL */     
  0, 1, P0,   /* P1OP, G1OP,   CLOSE */         
  0, 2, P2,   /* P2OP, G2OP,   READ */          
  0, 3, P2,   /* P3OP, G3OP,   READ_LINE */     
  0, 2, P0,   /* P2OP, G2OP,   WRITE */         
  0, 3, P0,   /* P3OP, G3OP,   WRITE_LINE */    
  0, 1, 0,    /* P1OP, G1OP,   CONNECT */       
  0, 0, 0,    /* P0OP, P0OP,   DISCONNECT */    
  0, 1, 0,    /* P1OP, G1OP,   SEND_NO_RESP */  
  0, 2, 0,    /* P2OP, G2OP,   SEND_RESP */     
  0, 2, 0,    /* P2OP, G2OP,   RECEIVE */       
  0, 1, 0,    /* P1OP, G1OP,   CANCEL */        
  0, 1, P0,   /* P1OP, G1OP,   NAV */           
  1, 0, P0,   /* P0OP, P0OP,   NAV_NEXT */      
  1, 0, P0,   /* P0OP, P0OP,   NAV_BACK */      
  1, 0, P0,   /* P0OP, P0OP,   NAV_FIRST */     
  1, 0, P0,   /* P0OP, P0OP,   NAV_LAST */      
  0, 1, P0,   /* P1OP, G1OP,   FETCH */         
  0, 2, P2,   /* P2OP, G2OP,   RETCHRQ */       
  0, 1, 0,    /* P1OP, G1OP,   OPEN_WINDOW */   
  0, 1, 0,    /* P1OP, G1OP,   OPEN_WINDOW */   
  0, 0, 0,    /* P0OP, P0OP,   CLOSE_WINDOW */  
  0, 1, 0,    /* P1OP, G1OP,   CL_OP_WINDOW */  
  0, 1, 0,    /* P1OP, G1OP,   KILL */          
  0, 0, P0,   /* P0OP, P0OP,   PURGE */         
  0, 2, P2,   /* P2OP, G2OP,   MOVE */          
  0, 2, P2,   /* P2OP, G2OP,   MOVE_ABS */      
  0, 2, P12,  /* P2OP, G2OP,   SWAP */          
  0, 2, P2,   /* P2OP, G2OP,   ADD */           
  0, 2, P2,   /* P2OP, G2OP,   SUB */           
  0, 2, P2,   /* P2OP, G2OP,   MUL */           
  0, 2, P2,   /* P2OP, G2OP,   DIV */           
  0, 3, P2,   /* P3OP, G3OP,   DIV_REM */       
  0, 3, P1,   /* (P1OP | PAR3OP), (G1OP | GLB3OP),     /* FILL */
  0, 2, P2,   /* P2OP, G2OP,   AND */    
  0, 2, P2,   /* P2OP, G2OP,   OR */     
  0, 2, P2,   /* P2OP, G2OP,   XOR */    
  0, 2, P0,   /* P2OP, G2OP,   TEST */   
  0, 2, P0,   /* P2OP, G2OP,   LEN */    
  0, 3, P3,   /* P3OP, G3OP,   FORMAT */ 
  2, 0, P0,   /* PARENBALL, GLBENBALL,    MAK_FMT */
  2, 0, P1,   /* PARENBALL, GLBENBALL,    EDIT (see edit.c) */
  2, 0, P1,   /* P2OP, G2OP,   STRING (see str.c) */  
  0, 4, P2,   /* P4OP, G4OP,   SUBSTR */              
  0, 3, P3,   /* P3OP, G3OP,   INSTR */               
  0, 1, P1,   /* P1OP, G1OP,   UPPER */               
  0, 1, P0,   /* P1OP, G1OP,   PUSH */                
  0, 1, P1,   /* P1OP, G1OP,   POP */                 
  0, 3, 0,    /* P3OP, G3OP,   SYNC_SAV */            
  0, 1, 0,    /* P1OP, G1OP,   SYNC_RELO */           
  0, 1, 0,    /* P1OP, G1OP,   TIMER */               
  0, 0, 0,    /* P1OP, G1OP,   WAIT */                
  0, 4, 0,    /* P4OP, G4OP,   START */               
  0, 1, 0,    /* P1OP, G1OP,   STOP */                
  0, 3, 0,    /* P3OP, G3OP,   SET_KEY */             
  0, 5, 0,    /* P5OP, G5OP,   SET_KEY_PRG */         
  0, 2, 0,    /* P2OP, G2OP,   SET_FUNC */            
  0, 3, 0,    /* P3OP, G3OP,   SET_FUNC_PRG */        
  2, 0, 0,    /* PARENBALL, GLBENBALL,    CALL */
  2, 0, 0,    /* (PARENBALL & ~P1OP), (GLBENBALL & ~G1OP),    LINK */
  1, 0, 0,    /* P0OP, P0OP,   RTN */        
  1, 0, 0,    /* P0OP, P0OP,   TRANSFER */   
  1, 0, 0,    /* P0OP, P0OP,   EXIT */       
  0, 1, 0,    /* P1OP, G1OP,   GO_DEPEND */  
  0, 1, 0,    /* P1OP, G1OP,   ERROR */      
  1, 0, 0,    /* P0OP, P0OP,   SAVEFIELD */  
  0, 3, 0,    /* P3OP, G3OP,   SAVEFIELDS */ 
  0, 2, 0,    /* P2OP, G2OP,   RESTORE */    
  0, 1, 0,    /* P1OP, G1OP,   RELEASE */    
  1, 0, 0,    /* P0OP, P0OP,   CLEAR FIEL */ 
  0, 2, 0,    /* P2OP, G2OP,   CLEAR FLDS */ 
  0, 2, P0,   /* P2OP, G2OP,   NOTE FILE */  
  0, 2, P0,   /* P2OP, G2OP,   POINT FIL */  
  0, 0, 0,    /* P0OP, P0OP,   SOUND */      
  0, 2, 0,    /* P2OP, G2OP,   SET SOUND */  
  0, 3, 0,    /* P3OP, G3OP,   SORT */       
  0, 5, 0,    /* PARENBALL, GLBENBALL  LOOKUP */
  0, 1, 0,    /* P0OP, P0OP,   SET_BACKGRND */ 
  0, 1, 0,    /* P1OP, G1OP,   TRIG_FUNC */    
  0, 1, 0,    /* P1OP, G1OP,   FILE_SCREEN */  
  0, 1, 0,    /* P1OP, G1OP,   SHOW_SCREEN */  
  0, 2, 0,    /* P2OP, G2OP,   UPLOAD */       
  0, 2, 0,    /* P2OP, G2OP,   DOWNLOAD */     
  0, 2, 0,    /* P2OP, G2OP,   ACCESS */       
  1, 0, 0,    /* P0OP, P0OP,   NOP */          
  0, 1, 0,    /* P1OP, G1OP,   TIMER_OFF */    
  1, 0, P2,   /* PARENBALL, GLBENBALL  MOVE_BLOCK */
  0, 1, 0,    /* P1OP, G1OP,   RTN */          
  0, 1, 0,    /* P1OP, G1OP,   EXIT */         
  0, 0, 0,    /* P0OP, P0OP,   REFRESH */      
  0, 1, 0,    /* P1OP, G1OP,   ERASE */        
  0, 1, 0,    /* P1OP, G1OP,   SET_CURSOR */   
  1, 0, 0,    /* P0OP, P0OP,   BRK */          
  1, 0, 0,    /* P0OP, P0OP,   BRK */          
  0, 1, 0,    /* P1OP, G1OP,   OPENERRWIN */   
  0, 7, 0,    /* P7OP, G7OP,   DEF_PGR_PARAM */
  0, 4, 0,    /* P4OP, G4OP,   SET_FUNC_PRG */ 
  0, 2, 0     /* P2OP, G2OP    OPENERRWIN */   
};
