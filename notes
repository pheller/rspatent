December 11th 2016
When Prodigy filed for patents on its reception system (i.e. the client side of Prodigy that's saved on your 286 as PRODIGY.EXE), they included source code as an appendix.

The patent office provides PDF scans of these appendices.  The goal of this project is to transcibe this into files more resembling its original form.

These files could then be used as a searchable compendium for those who wish to disassemble the Prodigy binary.

Perhaps these files could even be compiled into a somewhat working binary.

But, there could be pages or files missing.  There are 2 scans which are in different orders and seem to possibly have different subsets of the codebase.

Another limitation is that the PDF appears to cut off code past line position of about 76 characters.  Hopefully they were pretty good about not going past that limit.  Perhaps whatever monitors + text editor was used in 1987 would strongly encourage a person not to go past that length.

Each page has what appears to be a hole punch in the top left.  This can occlude up to 3 characters on each of 2 lines.  These can usually be deduced from surrounding text.

Each page has a vertical stamp on the left side: "07388156.092702".  This stamp is pretty light and can partially occlude 1 character on about 16 lines.  This generally isn't a problem, though it makes for tons of OCR noise.

Each page has a page number at the center bottom.

Each page has what appears to be a file path (sadly not including file name) at the bottom left.

There seems to be a pretty consistent slight slant (tilted a couple degrees) on the pages.

Maybe OCR could be improved with some training?  A 15 minute try produced pretty crappy results.  Manually typing is tedious, but it does force me to slow down and think a bit about the code.

Unclear why there were 2 separate scans.  All in all, each scan is about 1500 pages of code - (ANSI? Microsoft v4?) C, (8086?) Assembly, and some sort of makefiles.

I have downloaded the entire patent as a PDF and will be committing files one by one, referencing what page the file was transcibed from in case there are transcription errors that need to be corrected.

I'm trying to create an exact copy for posterity's sake and to avoid creating a derivative work (for now).

Being OCD about making an exact copy, style and all, has actually helped me catch typos while transcribing.

I think they had spacebars that randomly put spaces, such as a space before the semicolon ending a statement.

Closing curly braces often close one space to the right of where they open.

Files contain headers with comments about what they changed - comments we would put in source control today.  They probably didn't use a VCS in the early years.

Indentation is all over the place.  Scope can be indented 5 spaces, or 3 spaces, or...

Functions are declared using K&R syntax.

There is a variable 'nocom' which appears to correspond to GEV variable #30 SYS_NOCOMM.

Not sure if GEVs are 0 or 1 indexed.
