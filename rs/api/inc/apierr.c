/* API     = 1.1.2    LAST_UPDATE = 1.0.0    */
/***********************************************************************
*
* FILE NAME:    apierr.c
*
* DESCRIPTION:  Defines the API (TBOL interpreter) error messages.
*
* NOTE: the API message table starts at 1, not 0 and needs a dummy entry
* in the first location.  This results from the API using the fatal error
* message code synonymously with api_return_code, which needs to be 0 for
* verbs or programs which execute without error.
*
*      DATE           PROGRAMMER      REASON
* --------------------------------------------------------------------
*      20 AUG. '86     M. Silfen      Original
*      8/27/87         L. A.          Renumber to start at 1
************************************************************************
*
*                 **** API ERROR MESSAGES *****
*
************************************************************************/

extern   null_rtn();

ECB  api_err_table[] =
{
 "", null_rtn, /* DUMMY ENTRY - SEE ABOVE WARNING */
 "CONVERSION ERROR - non-numeric character detected.",null_rtn,
 "BAD OPERAND INDEX - decimal type index encountered.",null_rtn,
 "OPERAND INDEX OVERFLOW - index exceeded range of data ptr",null_rtn,
 "INVALID RUN TIME OFFSET - illegal operand id",null_rtn,
 "NO PARTITION VARIABLE - undefined partition variable operand",null_rtn,
 "PACKET ERROR - error in getting byte from program object.",null_rtn,
 "NO GLOBAL VARIABLE - undefined global variable operand.",null_rtn,
 "GET BUFFER ERROR - no memory resources available.",null_rtn,
 "OPERAND ERROR - illegal operand type.",null_rtn,
 "GLOBAL DISABLE - operand should not be a partition variable.",null_rtn,
 "RETURN INSTRUCTION PROCESS ERROR.",null_rtn,
 "CALL INSTRUCTION PROCESS ERROR.",null_rtn,
 "LINK INSTRUCTION PROCESS ERROR.",null_rtn,
 "MEMORY ALLOCATION ERROR (GETBUF MACRO).",null_rtn,
 "OBJECT REFERENCE ERROR (GETBYTE MARCRO).",null_rtn,
 "BAD SYSTEM DATE AND/OR TIME",null_rtn
};
