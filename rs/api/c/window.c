/* API     = 1.1.2    LAST_UPDATE = 1.0.0    */
/*---------------------------------------------------------------------
**    WINDOW.C       Window Verbs
**--------------------------------------------------------------------
**
**   open_wind()       OPEN_WINDOW
**   close_wind()      CLOSE_WINDOW
**   close_open_wind()  OPEN_ERROR_WINDOW
**
**    DATE          PROGRAMMER     REASON
**--------------------------------------------------------------------
**  05/26/86        R.G.R.         ORIGINAL
**  09/18/86        R.D.C.         Updated open_err_wind() for 4.0
**  09/18/86        R.D.C.         Added open_err_wind_nam() for 4.0
**  09/23/86        R.D.C.         Added Op_ew_fn and support in
**                       opn_e_wind and opn_e_w_name.
**  07/30/87        SLW       Rewrite
**   2 /18/88       lrz            save the current W in the api stack
**-------------------------------------------------------------------*/

#include <WINDOW.LNT>         /* lint args (from msc /Zg) */
#include <stdio.h>
#include <SMDEF.IN>      /* Service manager definitions. */
#include <DEBUG.IN>      /* Debugger compiler switch. */
#include <INSERR.IN>          /* Instruction error return codes. */
#include <RTADEF.IN>          /* Run time array definitions. */
#include <RTASTR.IN>          /* Run time array structure */
#include <OPERDEF.IN>         /* Operand definitions. */
#include <OPERTSTR.IN>        /* Operand definitions */
#include <OBJUNIT.IN>         /* Object unit structure. */
#include <SMPPT.IN>      /* Service manager page partition templ*/
#include <ISCB.IN>       /* Call,link:RTN stack definition. */
#include <SSCB.IN>       /* Window stack control block struct. */
#include <APIUTIL.IN>         /* API utility externs */

/*---------------------------------------------------------------------
**            Externals
**-------------------------------------------------------------------*/

extern SSCB *api_suspend;        /* Current api suspend stack ptr. */
extern SSCB sscb_stack[];        /* API window stack. */

extern unsigned char apirtn;     /* Return to reception system flag.*/
extern int Sys_current_cursor_pos;
extern ISCB *level0;             /* Level0 return pointer. */
extern ISCB *instack;

#if DEBUG
extern unsigned char debug_re_entry; /* Debug re entry flag. */
#endif

/*---------------------------------------------------------------------
**            Definitions
**-------------------------------------------------------------------*/

#define NORMAL_WINDOW       0
#define ERROR_WINDOW        1
#define CLOSE_OPEN_WINDOW  2

#define RTN            1
#define NORTN          0

/*---------------------------------------------------------------------
**            Variable Declarations
**-------------------------------------------------------------------*/

/*---------------------------------------------------------------------
**  proc_window(window_flag)
**
**   Attempt to open a window.  The flag indicates whether an error or
**   non error window is to be opened.
**
**    DATE           PROGRAMMER    REASON
**--------------------------------------------------------------------
**  05/16/86        R.G.R.         ORIGINAL
**-------------------------------------------------------------------*/

proc_window(window_flag)
unsigned int window_flag;
{
    /* Initialize pointers. */
#define window_id (&optbl[0])

#if DEBUG
    /* Validate operands. */
    if (window_id->op_type <= REG_TYPE)
    {
     api_rtn_code = OPER_ERR;
     return;
    }
#endif

    /* Queue pointer to window object id into OP process queue. */
    op_w_queuebuf(windows_id->buffer, window_id->buflen);

    /* Save environment if normal window is to be opened. */
    if (window_flag == NORMAL_WINDOW)
    {
     /* Save stream pointer and ip for return. */
     --api_suspend;
     api_suspend->s_opcode = *ip;
     api_suspend->s_stream = stream_ptr;
     api_suspend->s_offset = Obj_get_offset (stream_pointer);
     api_suspend->w_level0 = level0;
     api_suspend->api_event = Sys_api_event;
     api_suspend->save_w = W; /* lrz 2/18/88 */
     level0=instack;
    }

    /*Deleted close window code. 10:10:86 R.G.R*/
    else if (window_flag != CLOSE_OPEN_WINDOW)
     /*Inform KM to open an error window*/
     km_errorwindow();

    /* Inform KM to open a window. */
    km_openwindow();

    /* Set flag to return to reception system. */
    apirtn = RTN;

#if DEBUG
    if (window_flag != CLOSE_OPEN_WINDOW)    /*   lz - 6/30/87  */
#endif

#undef window_id
}

/*---------------------------------------------------------------------
**  open_wind()
**
**   API OPEN_WINDOW verb. Attempt to open a window.  The window
**   object-id is the first and only operand of this instruction.
**
**  Instruction format:
**
**   instruction       operand#1
**    open_window      window-id
**
**    DATE           PROGRAMMER    REASON
**--------------------------------------------------------------------
**  05/16/86        R.G.R.         ORIGINAL
**-------------------------------------------------------------------*/

open_wind()
{
    /* Call to process this normal open window request. */
    proc_window(NORMAL_WINDOW);
}

/*---------------------------------------------------------------------
**  opn_e_wind()
**
**   API OPEN_ERROR_WINDOW verb. Attempt to open a window.  The window
**   object-id is the first and only operand of this instruction.
**
**  Instruction format:
**
**   instruction       operand#1
**    open_window      window-id
**
**    DATE           PROGRAMMER    REASON
**--------------------------------------------------------------------
**  05/26/86        R.G.R.         ORIGINAL
**  09/18/86        R.D.C.         Changed Sys_current_field to
**                         Sys_current_cursor_pos.
**  09/23/86        R.D.C.         added support for Op_ew_fn.
**-------------------------------------------------------------------*/

opn_e_wind()
{
    PVAR *pentry;
    FIELD *field_entry;

    /* Call to process thia error window request. */
    proc_window(ERROR_WINDOW);

    op_set_ew_fn(-1);  /* No field specified for cursor location */
}

/*---------------------------------------------------------------------
**  opn_e_w_name()
**
**   API OPEN_ERROR_WINDOW verb.  Attempt to open a window. The
**   window object-id is the first operand and the name of the field
**   in error is the second operand of this instruction.
**
**  Instruction format:
**
**   instruction       operand#1    operand #2
**    open_window      window-id    name
**
**    DATE           PROGRAMMER    REASON
**--------------------------------------------------------------------
**  05/26/86        R.G.R.         ORIGINAL
**  09/18/86        R.D.C.         Changed Sys_current_field to
**                         Sys_current_cursor_pos.
**  09/23/86        R.D.C.         added support for Op_ew_fn.
**-------------------------------------------------------------------*/

opn_e_w_name()
{
    PVAR *pentry;
    REG int name;

    /* Call to process thia error window request. */
    proc_window(ERROR_WINDOW);

    /* Get name of field in error */
#define field_name (&optbl[1])
    name = get_int_from_rta((RTA *)field_name, field_name->op_type);

    /* Place current field in error. */
    if (((int *)pentry = loc_pev(name)) == NULL)
    {
#if DEBUG
     api_rtn_code = OPER_ERR;
#endif
     return;
    }

    /* Place current field in error. */
    op_set_not_fired(pentry, name);
#undef field_name
}

/*---------------------------------------------------------------------
**  close_wind()
**
**   API CLOSE_WINDOW verb.   Attempt to close a window.
**
**  Instruction format:
**
**   instruction
**    open_window
**
**    DATE           PROGRAMMER    REASON
**--------------------------------------------------------------------
**  05/26/86        R.G.R.         ORIGINAL
**-------------------------------------------------------------------*/

close_wind()
{
    /* Inform KM that a window has closed. */
    km_closewindow();

    /*Return to Service Manager*/
    apirtn = RTN;
}

/*---------------------------------------------------------------------
**  close_open_wind()
**
**   API CLOSE_WINDOW verb.   Attempt to close a window and then
**   open a window.
**
**  Instruction format:
**
**   instruction    operand#1
**    open_window    window-id
**
**    DATE           PROGRAMMER    REASON
**--------------------------------------------------------------------
**  05/26/86        R.G.R.         ORIGINAL
**-------------------------------------------------------------------*/

close_open_wind()
{
    /* Inform KM that a window has closed. */
    km_closewindow();

    /* Open window the specified window. */
    proc_window(CLOSE_OPEN_WINDOW);
}
