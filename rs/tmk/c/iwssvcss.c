/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
/********************************************************************/
/*                                          */
/* IWSSVCSS - task creation support routines               */
/*                                          */
/*    DESCRIPTION - creates a task                               */
/*                                          */
/*    HOW INVOKED - called without arguments or return codes.    */
/*        Communication is through global variables.       */
/*                                          */
/*    INPUTS -                              */
/*        callsfv: the resource (sfv) number on create           */
/*                                          */
/********************************************************************/
#include <iwsdefsc.h>
#include <iwssvccc.h>
#include <iwssvcsc.h>

/* external variables */

extern unsigned char far *callptr;
extern DQE *zsyssqh,*zsysuqh,*zsyseqh;
extern DQE *svcdqeptr;   /* pointer to DQE entry */
extern SFV *svcsfvptr;   /* pointer to requested SFV entry */
extern int callsfv;

/* external routines */

extern svcabend();

/* local variables - static for shared access and smaller stack */

static DQE **qp;    /*  note pointer to pointer */
static DQE *dqp,*dqp1;
static int i;

/********************************************************************/
/* svcmgrmk - create a manager (task). This works similar to a     */
/*     semaphore creation, except the task has more information to  */
/*     be initialized and the task must be inserted into the proper */
/*     task queue. For a user task, the task goes at the front of   */
/*     the queue. For the other two types, it goes in at sorted     */
/*     order (increasing priority number). If the priority matches  */
/*     another task, the task goes after the earlier one. Note that */
/*     ordering is permanent so the other task always gets prefered */
/*     dispatching. The dispatcher changes the "front" of the user  */
/*     queue, so where a task ends up in relation to other user     */
/*     tasks depends on where the dispatcher is in its round robin. */
/*     A task starts out nondispatchable.                  */
/*                                          */
/********************************************************************/
void svcmgrmk()
{
   if ((svcdqeptr = zsyseqh) == 0)  /* get a free dqe    */
      svcabend(NODQE);          /* reached dqe limit */
   zsyseqh = zsyseqh->zdqenext;   /* first entry removed from chain */
   svcdqeptr->zdqedisp = QNODISP; /* starts non dispatchable */
   svcdqeptr->zdqeflag = ((struct sfvplst far *)callptr)->sf_susp;
   svcdqeptr->zdqeprio = ((struct sfvplst far *)callptr)->sf_prty;

   /* copy stack pointer */

   (int *)(svcdqeptr->zdqestak) =
                   ((struct sfvplst far *)callptr)->sf_stkad;
   *((int *)(&sfvdqeptr->zdqestak) + 1)  =
                    ((struct sfvplst far *)callptr)->sf_stkss;

   /* clear the request and completion queues */

   svcdqeptr->zrqhfrt = sfvdqeptr->zrqhlst =
      svcdqeptr->zcqhfrt = svcdqeptr->zcqhlst = 0;
   svcdqeptr->zdqesfv = callsfv;    /* remember my id */
   svcdqeptr->zdqeact = 0;        /* never been active */

   /* fill in the sfv for this task, such as back ptr to the dqe */

   svcsfvptr->sfvu.sfvd.zsfvdqea = (unsigned char *)svcdqeptr;
   svcsfvptr->zsfvtag = QSFVTMGR;

   /* add to the proper task queue */

   if (svcdqeptr->zdqeprio <= 0x7F) {   /* system queue */
      svcsfvptr->sfvu.sfvd.zsfvmant = QSFVSYSMAN;
      if ((dqp = zsyssqh) == 0) {  /* queue is empty */
      zsyssqh = svcdqeptr->zdqenext = svcdqeptr;
      }
      else {     /* add in at the right place */
      if (zsyssqh->zdqeprio > svcdqeptr->zdqeprio) {
         zsyssqh = svcdqeptr;      /* we are now the front */
      }
      queuemid();        /* take care of the details */
      }
      }
   else if (svcdqeptr->zdqeprio = 0x80) {
      svcsfvptr->sfvu.sfvd.zsfvmant = QSFVUSERMAN;
      if ((dqp = zsysuqh) == 0) {   /* queue is empty */
      zsysuqh = svcdqeptr->zdqenext = svcdqeptr;
      }
      else {     /* add in at the right place */
      while (dqp->zdqenext != zsysuqh) {    /* find end to fix its ptr */
            dqp = dqp->zdqenext;
      }
      svcdqeptr->zdqenext = zsysuqh;        /* point to old front */
      dqp->zdqenext = zsysuqh = svcdqeptr;  /* point old last to new 1st */
      }
      }
}

/********************************************************************/
/* queuemid - find the proper place in the specified queue to     */
/*     insert the new task. Upon entry: queue is nonempty, dqp =    */
/*     first entry on that same queue.                     */
/*    After    finding the place, go ahead and perform the insert. */
/*    Variable svcdqeptr = address of dqe to insert.       */
/*                                          */
/********************************************************************/
static queuemid()
{
  dqp1 = dqp;             /* start from queue origin */
  while (dqp1->zdqenext != dqp) {  /* find end = pointer to origin */
     dqp1 = dqp1->zdqenext;
  }
  qp = (DQE **)dqp;       /* remember head of chain */
 qmloop:
  if (dqp->zdqeprio <= svcdqeptr->zdqeprio) { /* existing prio <= mine */
     dqp1 = dqp;                    /* I go after this one   */
     if ((dqp = dqp->zdqenext) != (DQE *)qp)  /* ..unless reached end  */
     goto qmloop;
  }

  /* insert between dqp1 and dpq */

  svcdqeptr->zdqenext = dqp;
  dqp1->zdqenext = svcdqeptr;
}
