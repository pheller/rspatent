/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
/********************************************************************/
/*                                          */
/* IWSSVCSC - "C" structure definitions for SVC operations          */
/*                                          */
/*    DESCRIPTION - define structure layouts (no actual creation */
/*        of data) for use in generating SVC requests.     */
/*                                          */
/*    COMMENTS -                                 */
/*        1. Must be kept synchronized with SVC changes.        */
/*                                          */
/********************************************************************/
struct svcreqs {
   int sr_indx;           /* becomes ax, usually an index value */
   int sr_type;           /* SVC type                */
   int sr_spec;           /* other specification, usually 0    */
   };

/* define structure for svc plist where a name is the first entry  */
struct sfvplst {
   char sf_name[8];       /* name of entry in the table        */
   int    sf_indx;        /* index returned for entry     */
   int    *sf_stkad;           /* address of task stack         */
                     /* pointer = 32 bits large, 16 small */
#ifndef M_I86LM
   int    sf_stkss;       /* segment of task stack             */
#endif
   char sf_prty;          /* priority                */
   char sf_susp;          /* suspend status          */
   };

/* define structure for svc plist with normal svc function     */
struct sfvplst {
   int    sp_rqst;        /* request type                 */
   int    sp_rc;               /* return code                  */
      /* others vary with the function                    */
   int    sp_args[6];          /* 6th valid only if not rqe use    */
   };
#define _PLSTSIZ  16           /* size (bytes of svcplst       */

/* structure with register environment setup at task creation    */
struct env {         /* free stack space precedes "env"           */
   int en_es;        /* individual registers  */
   int en_ds;
   int en_di;
   int en_si;
   int en_bp;
   int en_dx;
   int en_cx;
   int en_bx;
   int en_ax;
   int en_csoff;
   int en_csseg;
   int en_psw;
   };


/* structure for values returned from a getrqst function         */
struct grqptrs {
   unsigned int tag;              /*  rqe tag value         */
   unsigned int dqeptr;      /* sender's dqe               */
   unsigned int dxval;            /* offset sender's parm list  */
   unsigned int esval;            /* segment sender's parm list */
};
#define _GPTRSIZ  8          /* size (in bytes of grqptrs */
/*******************************************************************/
/*   END IWSSVCSC                              */
/*******************************************************************/
