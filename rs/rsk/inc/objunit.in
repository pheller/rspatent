/* RSK     = 1.1.4    LAST_UPDATE = 1.0.0    */
/*  objunit.in  11/3/87 11:55 */
/*
     file:          objunit.in
     author:        Michael L. Gordon
     date:          3-June-1986
     facility: Object Manager

     Description:

          Definition of the object unit interface of the object manager.

     History:

          2/26/87        Michael L. Gordon
               Objreft structure modified: union varptr changed to
                    struct varptr.
               Obj_ref_kill changes OBJREFACCESSED to OBJREFNOTACCESSED.

          3/03/87        Michael L. Gordon
               Obj_ext_status function declaration added.

          4/07/87        Michael L. Gordon
               ObjGetAll function declaration added.

          5/20/87        MIchael L. Gordon
               Segregation of objtypes to separate interface.
*/
#include <version.in>

#ifndef OBJUNITDEF
#define OBJUNITDEF      1

/* Definition of object storage paths */
     extern    char *OStagePath;   /* stage */
     extern    char *OCachePath;   /* cache */
     extern    char *ODumpPath;         /* crash dump */
     extern    char *OTestPath;         /* test path */

/* Definition of the maximum object size for memory cache candidacy */
     extern    int       OMemCacheMaxObjSize;

/* set to nonzero for support of disk cache fetching */
#define ObjDiskCacheFetching  0

#ifndef FAR
#define FAR
#endif

#ifndef PROGRAM_CALL
#include  <sysunit.in>
#include  <objtypes.in>

#endif    /* PROGRAM_CALL not defined */

/* definition of field of extended status */
     typedef   unsigned int   ObjEStatus;

/* definition of the index identity of an object in the directory */
     typedef int             Objidxt;

/* definition of User Status Request changes */
     typedef   enum {
          ObjUserStatusNoRequest,       /* no request */
          ObjUserStatusCache,           /* request caching of this object */
          ObjUserStatusNoCache          /* request no caching of this object */
     } ObjUserStatus;

/* definition of the object stream - composition of this structure
   is not to be considered invariant.  The structure is defined here
   only to support typed references to it. */
     typedef struct {
          Objidxt         objectidx;
          char FAR        *curptr;
          int             curoff;
          int                 fenceoff;
     } Objcontext;
     typedef Objcontext      *Objstream;

/* definition of the object reference structure */
     typedef struct {
          int                  status;    /* status of this object reference */
#define OBJREFOBJUNDEFINED         0
#define OBJREFACCESSED        1
#define OBJREFNOTACCESSED     2
#define OBJREFFETCHED         3
#define OBJREFERROR          -1
          struct {
               Objectid                *objidptr;       /* pointer to the object */
               Objstream               streamptr;       /* stream pointer if a ?? */
          } varptr;
     } Objreft;

/* Public function ObjInit - initialization of the object management unit.
   Returns 0 if OK, else error
*/
#ifdef    LINT_ARGS
extern    int  ObjInit( void );
#else
extern    int  ObjInit();
#endif

/* Public function ObjShutdown - shutdown of the object management unit.
*/
#ifdef LINT_ARGS
extern    void ObjShutdown( void );
#else
extern    void ObjShutdown();
#endif

/* Public function Obj_ref_get - establish access to an object by object id.
   The function will ensure that an object stream is established for
   the specified object.
   Returns the stream pointer if ok, else the null stream pointer.
   The caller supplies the pointer of the object reference structure.
   The contents of the object reference structure are modified by this
   function.
extern    Objstream Obj_ref_get( objrefptr )
     Objreft        *objrefptr;
*/
#ifdef LINT_ARGS
extern    Objstream Obj_ref_get( Objreft * );
#else
extern    Objstream Obj_ref_get();
#endif

/* Public function Obj_ref_fetch - ensure that an object has been fetched.
   The fetch will result cause a subsequent Obj_ref_get to response as
   quickly as possible since any server object access will have been
   started.
   The caller supplies the pointer of the object reference structure.
   The contents of the object reference structure are modified by this
   function.
extern    int       Obj_ref_fetch( objrefptr )
     Objreft        *objrefptr;
*/
#ifdef LINT_ARGS
extern    int       Obj_ref_fetch( Objreft * );
#else
extern    int       Obj_ref_fetch();
#endif

/* Public function Obj_ref_kill - ensure that the object is not accessed and
   any prefetching is stopped.
   The object reference structure is modified.
extern    int       Obj_ref_kill( objrefptr )
     Objreft        *objrefptr;
*/
#ifdef LINT_ARGS
extern    int       Obj_ref_kill( Objreft * );
#else
extern    int       Obj_ref_kill();
#endif

/* public function Get_object - establish access to the specified object.
   Upon successful return the object stream is established and invoking
   Get_obj_byte will return the first byte of the object.
   Upon successful return the streamptr will be set for subsequent
   function calls requiring the object stream pointer.
   The object is considered accessed and all space of the object is locked
   in memory until the object is deaccessed.
   Returns 0 if OK, -1 if error.
extern    int  Get_object( objidptr, streamptr )
     Objectid  *objidptr;
     Objstream *streamptr;
*/
#ifdef LINT_ARGS
extern    int  Get_object( Objectid *, Objstream * );
#else
extern    int  Get_object();
#endif

/* public function Get_obj_byte - get the next byte of the object associated
   with the Objstream.
   Returns 0 if Ok, -1 if end of object, -2 if object not accessed
extern    int  Get_obj_byte( stream, byteptr )
     Objstream      stream;
     Bytet               *byteptr;
*/
#ifdef LINT_ARGS
extern    int       Get_obj_byte( Objstream, Bytet * );
#else
extern    int       Get_obj_byte();
#endif

/* public function Obj_seek - seek to a particular location of an object.
   The function moves the object pointer associated with stream to a new
   location that is offset bytes from the origin of the object.  The next
   operation on the object takes place at the new location.
   Origin must be one of the following:
          0    -    beginning of object
          1    -    current position of object pointer

   Returns 0 if OK, -1 if offset or origin invalid, -2 if object not accessed
extern    int  Obj_seek( stream, offset, origin )
     Objstream      stream;
     int                 offset, origin;
*/
#ifdef LINT_ARGS
extern    int  Obj_seek( Objstream, int, int );
#else
extern    int  Obj_seek();
#endif

/* public function Obj_get_offset - get the current offset of the object
   associated with stream
   Returns offset or -1 if error.
extern    int  Obj_get_offset( stream );
     Objstream      stream;
*/
#ifdef LINT_ARGS
extern    int  Obj_get_offset( Objstream );
#else
extern    int  Obj_get_offset();
#endif

/* public function Obj_get_size - get the size of the object
   associated with stream
   Returns size in bytes (including header) or -1.
extern    int  Obj_get_size( stream )
     Objstream      stream;
*/
#ifdef LINT_ARGS
extern    int  Obj_get_size( Objstream );
#else
extern    int  Obj_get_size();
#endif

/* public function ObjGetAll - ensure that all bytes of an object possibly
   being acquired are obtained
extern    void ObjGetAll()
*/
#ifdef LINT_ARGS
extern    void ObjGetAll( void );
#else
extern    void ObjGetAll();
#endif

/* public function Obj_get_status - get the status of an object
   Returns the status of the object.
    value meaning
       -1  not found
       0        found, fully assembled
       1        fetch started, no content available
       >1  assembly in progress, returns number of bytes assembled so far

extern    int  Obj_get_status( objidptr )
     Objectid       *objidptr;
*/
#ifdef    LINT_ARGS
extern    int  Obj_get_status( Objectid * );
#else
extern    int  Obj_get_status();
#endif

/* public function Obj_ext_status - get the extended status of an object
   Returns the status of the object.
    value meaning
       -1  not found
       0        found, fully assembled
       1        fetch started, no content available
       >1  assembly in progress, returns number of bytes assmebled so far

   Status set as follows:
     ObjEStatusChild          entry is a child

extern    int  Obj_ext_status( objidptr, statusptr )
     Objectid       *objidptr;
     ObjEStatus          *statusptr;
*/
#define   ObjEStatusChild 000001
#ifdef    LINT_ARGS
extern    int  Obj_ext_status( Objectid *, ObjEStatus * );
#else
extern    int  Obj_ext_status();
#endif

/* public function Obj_get_Mptr - get the real memory pointer for the
   current location of the object;  retRunLength is set to the number of
   bytes immediately available in contiguous space starting at the
   current location.
   Returns the memory pointer or the null pointer if stream context is
   invalid.
extern    Bytet     *Obj_get_Mptr( stream, retRunLength )
     Objstream stream;
     int            *retRunLength;
*/
#ifdef    LINT_ARGS
extern    Bytet     *Obj_get_Mptr( Objstream, int * );
#else
extern    Bytet     *Obj_get_Mptr();
#endif


/* public function ObjStatusRequest - specify advisory information for the
   specified object.
extern    int  ObjStatusRequest( objidptr, requestStatus )
          Objectid       *objidptr;
          ObjUserStatus  requestStatus;
*/
#ifdef LINT_ARGS
extern    int  ObjStatusRequest( Objectid *, ObjUserStatus );
#else
extern    int  ObjStatusRequest();
#endif

/* public function ObjInstallChild - install the child object if not already
   in the directory.  The stream is positioned at the start of the segment
   ( Segment Type = Imbedded Object ) in which the object exists.
   The object id of the child is returned in the location at retobjid;
   The requestedStatus will be applied to the entry as if a call has been
   made to the ObjStatusRequest function.
extern int     ObjInstallChild( stream, retobjid, requestStatus )
     Objstream      stream;
     Objectid       *retobjid;
     ObjUserStatus  requestStatus;
*/
#ifdef    LINT_ARGS
extern    int  ObjInstallChild( Objstream, Objectid *, ObjUserStatus );
#else
extern    int  ObjInstallChild();
#endif

/* public function Obj_deaccess - deaccess the object associated with stream.
   This function must be invoked to free resources associated with the
   object.  Once deaccessed, the stream is invalid and Get_object must be
   called to access any portion of the object again.
   Returns 0 if ok, -1 if error.
extern    int  Obj_deaccess( stream )
     Objstream      stream;
*/
#ifdef LINT_ARGS
extern    int  Obj_deaccess( Objstream );
#else
extern    int  Obj_deaccess();
#endif

/* public function Obj_rundown - ensure that the specified object and all
   imbedded objects are deaccessed.
extern    int  Obj_rundown( objidptr )
     Objectid  *objidptr;
*/

/* public function Obj_refto_id - convert an object reference to an objectid.
   On entrance, the object stream is positioned so that Get_obj_byte will
   return the prefix byte of the reference, followed by the remainder of the
   referencing information.
   Upon successful return, the object id will be returned in the structure
   pointed to by objidptr, and stream will be advanced past the reference.
   Returns 0 if OK, -1 if error.
extern    int  Obj_refto_id( stream, objidptr )
     Objstream      stream;
     Objectid       *objidptr;
*/
#ifdef LINT_ARGS
extern    int  Obj_refto_id( Objstream, Objectid * );
#else
extern    int  Obj_refto_id();
#endif

/* public function Obj_fetch - prefetch the specified object so that
   the accessing of the object with Get_object will probably find the
   object in local memory.
extern    int  Obj_fetch( objidptr )
     Objectid  *objidptr;
*/
#ifdef LINT_ARGS
extern    int  Obj_fetch( Objectid * );
#else
extern    int  Obj_fetch();
#endif

/* public function Obj_kill - abort the prefetching of the specified object
extern    int  Obj_kill( objidptr )
     Objectid  *objidptr;
*/
#ifdef LINT_ARGS
extern    int  Obj_kill( Objectid * );
#else
extern    int  Obj_kill();
#endif

#endif    /* OBJUNITDEF not defined */

/* end of objunit.in */
