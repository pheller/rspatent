/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
/********************************************************************/
/*                                          */
/* IWSDEFSC - "C" structure definitions and equates for multitasker */
/*                                          */
/*    DESCRIPTION - define structure layouts (no actual creation */
/*        of data) and constants for use in the C version of     */
/*        the multitasker.                            */
/*                                          */
/*    HOW INVOKED - via "#include <iwsdefsc.h>"                  */
/*                                          */
/*    COMMENTS -                                 */
/*        1. Some fields at the ends of the structures have been */
/*        removed as unnecessary. All other offsets remain the*/
/*        same. One exception: zdqenext moved up. Can't shrink*/
/*        RQE because of 14-byte copy of request parm on ATN. */
/*        2. In the RQE definition, the dqe pointer should be    */
/*        of type *DQE but we cannot have circular type defs, */
/*        except to your own type (i.e. RQE can have *RQE).   */
/*                                          */
/********************************************************************/
typedef struct ZRQENTRY {      /* request queue element     */
   unsigned int zrqerqst;     /* svc request type       0 */
   unsigned char *zrqedqep;   /* requester's dqe address         2 */
   struct ZRQENTRY *zrqenxtp; /* next rqe pointer       4 */
   unsigned int zrqeprty;     /* request priority       6 */
   unsigned char far *zrqeplst; /* pointer to parm list      8 */
   unsigned int zrqenum; /* unused           12 */
} RQE;

typedef struct ZDQENTRY {      /* dispatch queue element        */
   unsigned int zdqedisp;     /* dispatch status        0 */
   unsigned char zdqeprio;    /* priority               2 */
   unsigned char zdqeflag;    /* dispatch flags         3 */
   unsigned char far *zdqestak;  /* current stack pointer    4 */
   RQE          *zrqhfrt;    /* request queue first          8 */
   RQE          *zrqhlst;    /* request queue last          10 */
   RQE          *zcqhfrt;    /* complete queue first        12 */
   RQE          *zcqhlst;    /* complete queue last         14 */
   unsigned int zdqesfv;      /* dqe's sfv index                16 */
   unsigned int zdqeact;      /* active counter        18 */
   struct ZDQENTRY *zdqenext; /* next dqe on dispatch chain 20 */
} DQE;

/* DQE constants for dispatch state */

#define QNODISP  0
#define QDISP   1
#define QWAIT   2
#define QUSET   3
#define QSEMWAIT 4
#define QSUSPND  0
#define QNOSUSP  1

typedef struct ZSFVNTRY {      /* SFV manager/function/sema */
   unsigned int zsfvtag;      /* sfv entry type         0 */
   union {
      int (far *zsfvepa)();      /* entry point          2 */
      struct {
      unsigned char *zsfvdqea;   /* dqe address or queue ptr    */
      unsigned int zsfvmant;     /* queue holding manager      */
      } sfvd;
   } sfvu;
   /* size is 6 bytes - resource name removed */
} SFV;

/* types of sfv tags */
/* manager (task), semaphore or free.                      */

#define QSFVTMGR 0
#define QSFVTSEM 2
#define QSFVTFREE 0xFF

/* which queue task is on */
#define QSFVSYSMAN 1
#define QSFVUSERMAN 2
#define QSFVWAITMAN 3

/* structure for mapping saved registers in an interrupt handler */

typedef struct OURREGS {
  unsigned int sares;
  unsigned int sards;
  unsigned int sardi;
  unsigned int sarsi;
  unsigned int sarbp;
  unsigned int sardx;
  unsigned int sarcx;
  unsigned int sarbx;
  unsigned int sarax;
  unsigned int sarroff;
  unsigned int sarrseg;
  unsigned int sarpsw;
} IWSREGS;

/* size of internal interrupt service stack */

#define SIZ_INT_STK 64

/* timer constants */

#define QTIMADJ 16

#define QSVCINDX 0x0103

/* abend codes */

#define ILLEGSFV 1
#define SVCNORQE 2
#define WRMANOP  3
#define WRSEMOP  4
#define NOTSEMWAIT 5
#define ILLEGSVC 6
#define DUPLINAME 7
#define NOSFV   8
#define NODQE   9
#define ILLEGTIM 10
#define NOTOWNTIM 11
#define SWAPCRAPS 12
#define MANHASQ   13
#define SEMREMERR 14
#define FCVNOTFCV 15
#define ILLEGREM  16
#define MANREMERR 17
#define BRICK1   18
#define BADCMPRQ  19
  /* other abend codes added for purposes of distinction   */
#define SFVREMOV  21
#define SFVDSPT   22
#define SFVWDSPT  23
#define SFVQSEM   24
#define SFVCLIX   25
#define SFVRQCLM  26
#define SFVCNDCL  27
#define SFVREL   28
#define SFVCLAIM  29
#define SFVIATN   30
#define MANDSPT   32
#define MANWDSPT  33
#define SEMQSEM   34
#define SEMCLIX   35
#define SEMRQCLM  36
#define SEMCNDCL  37
#define SEMREL   38
#define SEMCLAIM  39
/********************************************************************/
/*   END IWSDEFSC                             */
/********************************************************************/
