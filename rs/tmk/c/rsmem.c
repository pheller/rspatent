/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
/********************************************************************/
/*                                          */
/*                 T R I N T E X  C O N F I D E N T I A L            */
/*                                          */
/*********************************************************************/
/* CHANGE HISTORY -                              */
/*   DATE    LEV       TITLE            AUTHOR              */
/* 08/04/86  000       RSMEM.C               L. WHEELER          */
/*********************************************************************/
/*                                          */
/*    DESCRIPTION                                */
/*        This module is a collection of subroutines        */
/*        providing o/s independent access to memory mgmt      */
/*        functions.                             */
/*                                          */
/*       GLOBAL ENTRY POINTS                          */
/*             get_memory - return the address of available memory */
/*        free_memory - return a piece of memory for reuse    */
/*                                          */
/*    EXTERNAL SUBROUTINES CALLED                     */
/*        s_getm - home-grown memory allocation            */
/*        s_frem - home-grown memory free                  */
/*        claimsem - TMK claim a semaphore            */
/*        relsem - TMK release a semaphore            */
/*                                          */
/*********************************************************************/
#include <rsystem.h>
extern unsigned char far *s_getm();
/*********************************************************************/
/* FUNCTION                                                      */
/*   get_memory - get a variable length block of memory          */
/*                                          */
/* INPUT                                    */
/*   size - number of bytes requested                 */
/*                                          */
/* RETURNS                                       */
/*   pointer to allocated space or 0 if none available     */
/*                                          */
/* SYNOPSIS                                      */
/*   unsigned char far *get_memory (size);                 */
/*   unsigned long size;                         */
/*********************************************************************/

unsigned char far *get_memory (size)
unsigned long size; /* number of bytes to get */
{
     char far *addr; /* ptr to allocated memory */

     claimsem (MEM_SEM);
     addr = s_getm(size);
     relsem (MEM_SEM);
     return (addr);
}
/*********************************************************************/
/* FUNCTION                                                      */
/*   free_memory - return a block of memory for reuse            */
/*                                          */
/* INPUT                                    */
/*   addr - address of memory to be returned               */
/*                                          */
/* SYNOPSIS                                      */
/*   int free_memory (addr);                     */
/*   unsigned char far *addr;                    */
/*********************************************************************/

int free_memory (addr)
unsigned char far *addr; /* address of memory to free */
{
     int error;

     claimsem (MEM_SEM);
     error = s_frem(addr);
     relsem (MEM_SEM);
     if (error) error = -1;
     return(error);
}
