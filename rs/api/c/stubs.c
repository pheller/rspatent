/* API     = 1.1.2    LAST_UPDATE = 1.0.0    */
/*---------------------------------------------------------------------
**    STUBS.C        Unimplemented API Verbs
**--------------------------------------------------------------------
**
**   timon()       TIMER_ON
**   syncrel()     SYNC_RELEASE
**   kill()          KILL
**   syncsv()      SYNC_SAVE
**   start()       START
**   stop()          STOP
**   cancel()      CANCEL
**   set_attr()    SET_ATTRIBUTE
**   error()       ERROR
**   invalid()     invalid instruction
**   erase()       ERASE GIVEN "DIVISION [FIELD/ELEMENT/PAGE]" (EAP)
**   set_cursor()  ON EXIT, PUT CURSOR IN GIVEN FIELD (EAP)
**
**    DATE           PROGRAMMER      REASON
**--------------------------------------------------------------------
**  04/10/86        P.M.J.         ORIGINAL
**  07/28/87        SLW       Rewrite
**-------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <SMDEF.IN>      /* Service manager definitions. */
#include <DOS.H>
#include <INSERR.IN>
#include <DEBUG.IN>
#include <OPERDEF.IN>
#include <OPERTSTR.IN>        /* Operand definitions. */
#include <SMKEYS.IN>
#include <RTADEF.IN>
#include <RTASTR.IN>
#include <OBJUNIT.IN>
#include <SMPPT.IN>
#include <GEV_DEFS.IN>        /* GEV definitions */
#include <APIUTIL.IN>         /* API utility externs */
#include <SM_GLBS.IN>

extern struct EVENT event;

#if DEBUG_ALONE            /* 09/18/86  C. Hsieh  */
/*---------------------------------------------------------------------
**            Externals
**-------------------------------------------------------------------*/
extern unsigned char *ip;        /* Current instruction pointer. */
extern Objstream stream_ptr;     /* Program stream pointer. */
extern unsigned char apirtn;     /* Return to reception system flag. */

/*---------------------------------------------------------------------
**            Definitions
**-------------------------------------------------------------------*/
#define DISABLE   0

#define RTN      1
#define NORTN    0

/*---------------------------------------------------------------------
**            Variable Declarations
**-------------------------------------------------------------------*/

unsigned char Password_echo_char;
unsigned char FAR *Key_Functions;
unsigned char key_function_tbl[256];

Objectid Op_w_queue;
Objectid Op_queue;


/*---------------------------------------------------------------------
**  timon()
**
**   The API TIMER_ON verb.
**
**   Instruction operand#1
**   ---------------------
**    timon         field-id
**
**    DATE           PROGRAMMER      REASON
**---------------------------------------------------------------------
**  04/10/86        P.M.J.          ORIGINAL
**-------------------------------------------------------------------*/

timon()
{}

/*---------------------------------------------------------------------
**  syncrel()
**
**   The API SYNC_RELEASE verb.
**
**   Instruction operand#1
**   ---------------------
**    syncrel     request-id
**
**    DATE           PROGRAMMER      REASON
**---------------------------------------------------------------------
**  04/10/86        P.M.J.          ORIGINAL
**-------------------------------------------------------------------*/

syncrel()
{}

/*---------------------------------------------------------------------
**  kill()
**
**   The API KILL verb.
**
**   Instruction operand#1
**   ---------------------
**    kill          field-id
**
**    DATE           PROGRAMMER      REASON
**---------------------------------------------------------------------
**  04/10/86        P.M.J.          ORIGINAL
**-------------------------------------------------------------------*/

kill()
{}

/*---------------------------------------------------------------------
**  syncsv()
**
**   The API SYNC_SAVE verb.
**
**   Instruction operand#1    operand#2  operand#3
**   --------------------------------------------------
**   syncsv        request-id program-data-id  state
**
**    DATE           PROGRAMMER      REASON
**---------------------------------------------------------------------
**  04/10/86        P.M.J.          ORIGINAL
**-------------------------------------------------------------------*/

syncsv()
{}

/*---------------------------------------------------------------------
**  start()
**
**   The API START verb.
**
**   Instruction operand#1    operand#2      operand#3 op#4
**   -------------------------------------------------------------
**   start         request-id time-interval  program-data-id state
**
**    DATE           PROGRAMMER      REASON
**---------------------------------------------------------------------
**  04/10/86        P.M.J.          ORIGINAL
**-------------------------------------------------------------------*/

start()
{}

/*---------------------------------------------------------------------
**  stop()
**
**   The API STOP verb.
**
**   Instruction operand#1
**   -----------------------
**   stop     response-id
**
**    DATE           PROGRAMMER      REASON
**---------------------------------------------------------------------
**  04/10/86        P.M.J.          ORIGINAL
**-------------------------------------------------------------------*/

stop()
{}

/*---------------------------------------------------------------------
**  cancel()
**
**   The API CANCEL verb.
**
**   Instruction operand#1
**   ---------------------
**   cancel        msg-id
**
**    DATE           PROGRAMMER      REASON
**---------------------------------------------------------------------
**  04/10/86        P.M.J.          ORIGINAL
**-------------------------------------------------------------------*/

cancel()
{}

error ()
{
    apirtn = RTN;
}

invalid()
{}

nop()
{}

timeslice()
{}

file_screen()
{}

show_screen()
{}

upload()
{}

download()
{}

set_back_grnd()
{}

timer_off()
{}

insert_fld()
{}

Do_function_event()
{}

#endif
