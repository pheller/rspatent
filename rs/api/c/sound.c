/* API     = 1.1.2    LAST_UPDATE = 1.0.0    */
/*---------------------------------------------------------------------
**    SOUND.C       Sound Verbs
**--------------------------------------------------------------------
**
**  Do the following API instructions:
**
**   SOUND     set_sound(), sound_default()
**
**  05/26/86        R.G.R.         ORIGINAL
**  07/28/87        SLW       Rewrite
**-------------------------------------------------------------------*/

#include <SOUND.LNT>          /* lint args (from msc /Zg) */
#include <SMDEF.IN>      /* Service Manager definitions */
#include <DEBUG.IN>      /* Debugger compiler switch */
#include <OPERDEF.IN>         /* Operand definitions */
#include <OPERTSTR.IN>        /* Operand structure */
#include <RTADEF.IN>          /* Run time array definitions */
#include <RTASTR.IN>          /* Run time array definitions */
#include <INSERR.IN>          /* Instruction error return codes */
#include <APIUTIL.IN>         /* API utility externs */

/*---------------------------------------------------------------------
**            Externals
**-------------------------------------------------------------------*/

/*---------------------------------------------------------------------
**            Definitions
**-------------------------------------------------------------------*/

/*---------------------------------------------------------------------
**            Variable Declarations
**-------------------------------------------------------------------*/
unsigned int sound_pitch;
unsigned int sound_duration;

/*---------------------------------------------------------------------
**  set_sound()
**
**   The API SOUND verb.  Produce a sound on the speaker.  The first
**   operand is the sound pitch and the second operand is the sound
**   duration.  Both of these operands replace the global defaults.
**
**  Instruction format:
**
**   instruction       operand#1     operand#2
**   -------------------------------------------
**     sound       pitch    duration
**
**    DATE           PROGRAMMER      REASON
**--------------------------------------------------------------------
**  05/26/86        R.G.R.          ORIGINAL
**-------------------------------------------------------------------*/
set_sound()
{
    /* Initialize pointers. */
#define pitch   (&optbl[0])
#define duration (&optbl[1])

    /* Set the defaults */
    sound_pitch = get_int_from_rta((RTA *)pitch, pitch->op_type);
    sound_duration = get_int_from_rta((RTA *)duration, duration->op_type);

    /* Call assembly function to make the sound */
    sound_default();

#undef pitch
#undef duration
}

/*---------------------------------------------------------------------
**  sound_default()
**
**   The API SOUND verb.  Produce a sound on the speaker using the
**   global defaults.
**
**  Instruction format:
**
**   instruction
**   -----------
**     sound
**
**    DATE           PROGRAMMER      REASON
**--------------------------------------------------------------------
**  05/26/86        R.G.R.          ORIGINAL
**-------------------------------------------------------------------*/

sound_default()
{
    /* Call assembly function to make the sound. */
    sound(sound_pitch, sound_duration);
}
