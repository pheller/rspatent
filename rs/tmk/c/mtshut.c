/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
/********************************************************************/
/*                                          */
/* MTSHUT - program to remove resident version of multitasker     */
/*                                          */
/*    DESCRIPTION - Issues the TMK svc to shut down the          */
/*             multitasker.  All activity must be idle.    */
/*             All program memory is returned to the DOS   */
/*             memory.                           */
/*                                          */
/*    HOW INVOKED - program load with no arguments              */
/*                                          */
/*    SUBROUTINES -                              */
/*        vstfpgm: computes size of program for terminate       */
/*                                          */
/********************************************************************/
#include <dos.h>
union REGS inregs,outregs;

main()
{
/* Issue the SVC to shutdown the multitasker, which cleans up and     */
/* returns its PSP address, which we need to delete it from memory.   */
/* All other activity must be idle, and the multitasker must be       */
/* resident at the time.                           */
   inregs.x.bx = 14;             /* shutdown request */
   int8x(0x78,&inregs,&outregs);   /* issue int 7a */
   vstfpgm(outregs.x.ax);        /* ax = psp address, release stg */
}
