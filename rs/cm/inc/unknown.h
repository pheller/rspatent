/* CM      = 1.1.2    LAST_UPDATE = 1.0.1    */
/*
 *    LA 03/28/88   Modify for 6.1.3 changes
 *    SS 12/14/87   Added defines for error handling
/*

/* Communication error handling timers definitions used within CM */


#define DEBUG_PKT   0         /*** Make = 1, to print # of all packets RCVD
                         and TRANSMITTEDt.  For debbugging ******/
#define DEBUG_ERR   0         /*** Make = 1, to print CM error messages
                         for debugging.                  ***********/
#define LINE_STATS  0         /* Set to 1 to compile line statistics output
                       routine */
#define E_NO_TIMER  -3        /* Can't create timer; returned by LOS */
#define NUMWAKTMRS  2         /* No. of WACK timers to be created . This
                       value follows transmit window size   */
#define MAXWACKS    4         /* Maximum # of WACKs sent before fatal err */
#define NOISEINTS   5         /* # of consecutive intervals noise count
                       threshold must be exceeded for fatal err */
#define MAXNOISE    255       
#define WACKTIME    100       /* WACK timer interval -> 10 seconds */    
#define NACKTIME    100       /* NACK timer interval -> 10 seconds */    
#define RXACTVTIME  50        /* RX activity timer   -> 5 seconds */     
#define TXACTVTIME  30        /* TX activity timer interval -> 3 sec  */ 
#define CARRTIME    30        /* Carrier drop timer interval -> 3 sec */
#define NOISETIME   600       /* Noisy line timer interval -> 1 min   */
#define NACKTMR     0         /* NACK timer ID in cm_tmr structure */
#define RXACTVTMR   1         /* RCVR ACTIVITY timer ID      */
#define TXACTVTMR   2         /* XMIT ACTIVITY timer ID      */
#define NOISETMR    3         /* STAT timer ID used in noisy line case*/
#define CARRTMR     4         /* Carrier loss timer ID      */
#define WACK1TMR    5         /* XMIT WACK timer ID for 1 of 2 packets
                       which require an ACK from TCS   */
#define WACK2TMR    6         /* XMIT WACK timer ID for second packet
                       which require an ACK from TCS   */
#define MAXTIMERS   7         /* CM max # of timers          */
#define WACKTMR         8          /* Used to address WACK1TMR and WACK2TMR*/
#define NO_DATA         0xffff     /* LA 3/21/88             */


struct {       /* CM  timers structure: */
     int id;                  /* Timer ID, assigned by LOS  */
     int in_prog;             /* 1 if timer wait in progress ...
                            cleared by TMK when timer expires */
     char in_use;             /* 1 if timer is being used   */
     unsigned char pkt_no;         /* Packet # related to timer */
     unsigned char stat_cnt;       /* Counts # of NACKs, WACKs .. etc
                            for a target packet        */
} cm_timer[MAXTIMERS];

/* Communication fatal errors reported to Service Manager   */

#define OUTOFMEM    0    /* Can't allocate memory      */
#define NOTIMER          1    /* Can't create/start timers   */
#define NOACKTMOUT  2    /* Receiver activity time out      */
#define NOTXACTIVITY     3    /* Transmitter activity time out */
#define CARRLOSS    4    /* Line carrier loss               */
#define NOISYLN          5    /* Noisy line detected         */
#define WACKTMOUT   6    /* TCS not responding              */
#define CALLWAIT    7    /* Call Waiting detected (not fatal) */

/*  Communication Subsystem line and memory errors          */

#define   SYNCER    0              /* Packet header synch err      */
#define   NOPKTM    1              /* Packet memory problem */
#define   NOMEM     2              /* Memory not available   */
#define ICTER  3              /* Interchr timeout      */
#define PKTTER 4              /* Packet timeout        */
#define   MSGTER    5              /* Message timeout       */
#define   RXOVRN    6              /* Receiver overrun      */
#define   NODCD     7              /* Carrier Loss           */
#define   BADCRC    8              /* Bad CRC               */
#define   PKNOER    9              /* Unknown packet number */
#define PARER  10             /* Receive parity error         */
#define FRMER   11                      /* Receive framing error        */
#define NOIMEM 12             /* No intrp memory       */
#define FREMEM 13             /* System memory free error   */
#define   FREIMEM   14                      /* Interrupt memory free error */
#define NERR   15             /* Size of err_db             */

#define   STMSK     0x001e              /* Line errors  mask  */
#define   MODMSK    0x000f              /* Modem errors mask  */

#define   SYSERR    -1
#define MAX_NOISE 255              /* Max noise count LA 3/23/88 */

struct mods{
          unsigned dcts : 1;  /* CTS change            */
          unsigned ddsr : 1;  /* DSR change            */
          unsigned teri : 1;  /* RI change             */
          unsigned ddcd : 1;  /* DCD change            */
          unsigned fill : 12;
};

struct lins{
          unsigned drdy  : 1; /* Rcve Data Ready       */
          unsigned ovrer : 1; /* Receiver Overrun err  */
          unsigned parer : 1; /* Receive Parity error  */
          unsigned frer  : 1; /* Rcve Framing ERror    */
          unsigned brint : 1; /* Break interrupt       */
          unsigned tempy : 1; /* Tx holding reg empty  */
          unsigned tsre  : 1; /* Transmitter empty     */
          unsigned fill  : 9;
};
