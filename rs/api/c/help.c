/* API     = 1.1.2    LAST_UPDATE = 1.0.0    */
/********************************************************************
*
*                **** PROCESS DEBUG HELP COMMAND ****
*
*********************************************************************
*
* FILE NAME:           HELP.C
*
* DESCRIPTION:
*
*
*     DATE            PROGRAMMER          REASON
* --------------------------------------------------------------------
*   11/05/85            R.G.R            ORIGINAL
*
**********************************************************************/
#include <stdio.h>
#include "conio.h"
/***********************************************************************
*
*                  **** DEFINITIONS ****
*
***********************************************************************/
#define F1KEY   0x3B
#define PRINT   1
#define SCREEN  0
/***********************************************************************
*
*                    **** EXTERNALS ****
*
***********************************************************************/
extern unsigned char psmode;
extern FILE outf;
/***********************************************************************
*
*                 **** VARIABLE DECLARATIONS ****
*
***********************************************************************/
unsigned char *hlptbl []=
{
{"L          - Loads pseudo-code and symbol file into system memory.\n\n"},
{"SB <loc>   - Sets a breakpoint at relative decimal location.\n\n"},
{"RB <loc>   - Removes a breakpoint at relative decimal location.\n\n"},
{"RAB        - Removes all breakpoints.\n\n"},
{"LB         - List all current breakpoints.\n\n"},
{"U <loc><n> - Unassemble TBOL pseudo-code starting at location for 'n'\n"},
{"             number of TBOL instructions.\n\n"},
{"DH or DA   - Displays number of bytes of variable data in hex (DH) or\n"},
{" <field-id>  ascii (DA), beginning at offset.  If offset and #bytes is\n"},
{" <offset>    ommitted then entire variable data will be displayed.\n"},
{" <# bytes>\n\n"},
{"MH or MA   - Modifies given field, old field length will be lost and\n"},
{"             a new field length will be assigned. If optional <index>\n"},
{"             is specified then the old field length will not be lost.\n\n"},
{"MP         - Modifies TBOL pseudo-code in hex.\n\n"},
{"T <nbr>    - Executes one TBOL instruction and displays unassembled.\n"},
{"             instruction and results of operation.  If optional <nbr>\n"},
{"             is specified then the trace is for <nbr> of instructions.\n\n"},
{"G          - TBOL execution begins at start location and ends at end\n"},
{"<start loc>  location. If only one location is specified then execution\n"},
{"<end loc>    will start at that location. If no location is specified then\n"},
{"             execution will start at the current instruction pointer. Note\n"},
{"             execution will stop at a breakpoint if detected prior to end\n"},
{"             location.\n\n"},
{"FH or FA   - Fills given field with new data, old length will not be lost.\n"},
{" <field-id>\n"},
{" <char>\n\n"},
{"Q          - Terminates debug session and returns control to DOS.\n\n\n"},
{"FUNCTION KEYS:\n\n"},
{"F1         - Debugger reset; returns control back to debugger for user command\n"},
{"             input.\n\n"},
{"F2         - Enable/Disable output to printer.\n\n"}
};
/********************************************************************
*
*                **** PROCESS DEBUG HELP COMMAND ****
*
*********************************************************************
*
* ROUTINE NAME:            prochelp ()
*
* DESCRIPTION:
*
*
*     DATE            PROGRAMMER          REASON
* --------------------------------------------------------------------
*   11/05/85            R.G.R            ORIGINAL
*
**********************************************************************/
prochelp()
{

unsigned char count;
unsigned char *hptr;
unsigned char key;

for (count = 0; count != 15; ++count)
{
     printw("%s", hptr);
     if (psmode == PRINT)
        fprintf(outf,"%s",hptr);
     hptr= hlptbl[count];
}

printf ("\nFOR MORE HIT ANY KEY. HIT F1 KEY TO ABORT.");

wait:

key = getch();

if ((key == 0) && (getch() == F1KEY))
{
   return;
}

for (; count != 33; ++count)
{
     printf("%s",hptr);
     if (psmode == PRINT)
        fprintf(outf,"%s",hptr);
     hptr= hlptbl[count];
}

}
