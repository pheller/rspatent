/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
/********************************************************************/
/*                                          */
/* MTRES - main program for resident version of multitasker     */
/*                                          */
/*    DESCRIPTION - the multitasker may either be linked into one*/
/*        of the application program set or it may be linked into*/
/*        a minimal program in order to make an independent     */
/*        resident version. This is that minimal program. It     */
/*        sets up the multitasker and does a terminate and      */
/*        remain resident to return to the system.              */
/*                                          */
/*    HOW INVOKED - program load with no arguments              */
/*                                          */
/*    SUBROUTINES -                              */
/*        iwsnip: multitasker init                    */
/*        vstmsiz: computes size of program for terminate       */
/*                                          */
/*    COMMENTS -                                 */
/*        1. Be sure to adjust stack and heap size to amt needed.*/
/*        Use exemod to adjust it.               */
/*                                          */
/********************************************************************/
#include <iwssvccc.h>
#include <iwssvcsc.h>
#include <rsystem.h>
#include <dos.h>
#define TIMER_TASK  5    /* see iwsinths.asm -- LH, 4/20/87 */

  struct stack {
     unsigned char stack [600];
     struct env envmnt;
  };
  struct stack tskstkl;
  unsigned char far *tagptr;
  unsgiend char tag[] = "IWSSVCG";
  extern int timer_task();
  extern int _psp;
main()
{
  union REGS inregs,outregs;
  struct SREGS segregs;
  int i, flag;

#if DEBUG
  printf("Load TMK symbols at %4X\n", _psp + 0x10);
#endif

  flag = 1; /* assume TMK loaded unless you find out otherwise */

/*
   Determine if multitasker seems to be loaded by checking int 7a.
   It points to the SVC handler, and at offset 2 is supposed to be
   the tag "IWSSVCG". Set a flag as follows:
       0 - not loaded (int 7a = 0)
       1 - loaded
       2 - something else has 7a (nonzero but didn't find the tag)
*/
   inregs.h.ah = 0x35;           /* get vector       */
   inregs.h.al = 0x7a;           /* svc handler      */

   intdosx(&inregs,&outregs,&segregs);   /* issue dos int */

   FP_SEG(tagptr) = segregs.es;
   FP_OFF(tagptr) = outregs.x.bx + 2;    /* where tag is */

   if (segregs.es == 0 && outregs.x.bx == 0) flag = 0;  /* not in */

   for (i=0; i < 7; i++, tagptr++)
     if (*tagptr != tag[i]) flag = 2;  /* something else has int 7A */

   if (flag == 1) {
     exit ();
   }

   iwsnip(); /* initialize tmk */

  /* define logical operating system semaphores */

  defsem(MEM_SEM);
  defsem(TIMER_SEM);
  defsem(SLEEP_SEM);

  /* define logical operating system timer task but don't dispatch it */

  tskstkl.envmnt.en_ds = 0;
  deftask (TIMER_TASK, (int far *)timer_task, &tskstkl.envmnt, 0x70);

  i = vstmsiz();     /* find out how much space we take up */

  inregs.x.ax = 0x3100;   /* and terminate and stay resident */
  inregs.x.dx = i;
  intdos(&inregs,&inregs);
}
_nullcheck() {};    /* routines not needed, so use stubs to disable */
_setargv() {};     /* See msc manual. */
_setenvp() {};
