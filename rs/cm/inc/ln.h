/* CM      = 1.1.2    LAST_UPDATE = 1.0.1    */
/*
 *   LA 03/29/88    Add INIT state for RX interrupt initialization
 *      SS 12/14/87 Added defines for error handling
 *
 *   SS 10/01/87    Added macros GETIVEC for DOS call 35, and others
 *             for more possible modem responses.
 *   SS 09/21/87    Changed names BUFLEN, and CMDLEN to MCMDLEN and
 *             MRESLEN for a better representation. Also changed
 *             size of MCMDLEN to 40 (Hayes cmd buf size) and
 *             MRESLEN to 60 to support modems that return
 *             fancy messages while they are dialing
*/

/* ln.h - Line control definitions                     */

#define   HUTSTX    0         /*  Search for STX               */
#define   HUNTHDR   1         /* Hunt for link header               */
#define   HUNTEND   2         /* Hunt for end of packet             */
#define PHY    3         /* Get a chr in line prog mode             */
#define INIT   4         /* Initialization state LA 03/29/88 */
#define PKT    0         /* Line is in packet mode             */

#define   MAXRX     1024      /* receiver buffer size        */
#define   DONE 1         /* rcvr interrupt status      */
#define UDONE  0         /* rcvr interrupt status      */

                    /* HAYES modem status                 */
#define MOK    1         /* command executed correctly           */
#define MCON3  2               /* 300 bps connection                   */
#define MCON12      3               /* 1200 bps connection                  */
#define MCON24      4               /* 2400 bps connection                  */
#define MCON48      5               /* 4800 bps connection                  */
#define MCON96      6               /* 9600 bps connection                  */
#define MNOCAR 7               /* no carrier detected                  */
#define MERR   8               /* command not executed correct.        */
#define MNODTN 9               /* no dial tone detected                */
#define MBUSY  10              /* line is busy                         */
#define MNOANSR     11              /* phone rings but no answer            */
#define MDCON  12              /* modem disconnect due to lose of CD   */
#define HAYESTXID 13            /* HAYES Trippe X found      */

                    /* CONNECT command parameters         */
#define COM1   0               /* PC COM1 is used (default)            */
#define COM2   1               /* PC COM2 is used with service         */
#define EITH   3               /* data word bit length = 8 (default)   */
#define SEVEN  2               /* data word bit length = 7             */
#define TWO    1               /* selects 2 stop bits (default)        */
#define ONE    0               /* selects 1 stop bit                   */
#define NOPAR  0               /* no parity (default)                  */
#define ODD    1               /* odd parity                           */
#define EVEN   3               /* even parity                          */
#define PULSE  'P'             /* use pulse dialing                    */
#define TONE   'T'             /* use touch tone dialing (def)         */
#define HAYES5 0               /* HAYES with 0-5 result codes support(def)*/
#define HAYES10     1               /* HAYES with 0-10 result codes support */
#define HAYESTX     2               /* HAYES Triple X modem                 */
#define NULLMOD     3               /* NULL modem                           */
#define PC     0               /* IBM PC, XT, AT type               */

#define CR     0x0d      /* Carriage Return               */
#define LF     0x0a
#define MCMDLEN     40        /* modem command buffer lengths     */
#define MRESLEN     60        /* modem response buffer lengths     */

                    /* UART Interrupt Enable Registers bits */
#define RXION  0x1            /* enable Data Available Interrupt  */
                    /* Upper nibble always = 0         */
#define RXIOF  0xe             /* disable Data Available Int.          */
#define TXION  0x2       /* enable Tx Holding Reg           */
#define TXIOF  0xd             /* disable Tx Holding Reg               */
#define LSION  0x4            /* enable Rcvr Line Status Int  */
#define LSIOF  0xb             /* disable Rcvr Line Status Int.        */
#define MSION  0x8            /* enable Modem Status Int     */
#define MSIOF  0x7             /* disable Modem Status Int.            */
#define INTON  0xf            /* enable all interrupts      */
#define INTOF  0x0       /* disable all interrupts          */
#define DLABMSK 0x7f          /* enable interrupts pre_enable         */
#define DLABON  0x80            /* disable interrupts pre_enable        */
#define DLABL3 0x80           /* Least Divisor Latch Byte for  300 bps*/
#define DLABH3 0x01           /* high Divisor Latch Byte for  300 bps */
#define DLABL12     0x60           /* Least Divisor Latch Byte for 1200 bps*/
#define DLABH12     0x00           /* High Divisor Latch Byte for 1200 bps */
#define DLABL24     0x30
#define DLABH24     0x00
#define DLABL48     0x18
#define DLABH48     0x00
#define DLABL96     0x0c
#define DLABH96     0x00

                    /* UART Modem Control Registers bits    */
#define DTRON  0x1       /* enable DTR                           */
#define DTROF  0xfe            /* disable DTR                          */
#define RTSON  0x2       /* enable RTS                           */
#define RTSOF  0xfd            /* disable RTS                          */
#define OUT1ON 0x4             /* enable OUT1                          */
#define OUT1OF 0xfb            /* disable OUT1                         */
#define OUT2ON 0x8             /* enable OUT2                          */
#define OUT2OF 0xf7            /* disable OUT2                         */
#define LOOPON 0x10            /* enable local loop back               */
#define LOOPOF 0xef            /* disable local loop back              */
#define DTRTSON     0xb             /* enable DRT & RTS                     */
#define CDMASK 0x08            /* Modem status Reg CD bit change bit   */
#define CDSTAT 0x80            /* Modem Status Reg CD on/off bit     */

i                   /* 8259 Interrupt Controller parameters */
#define IMR_AD  0x21          /* 8259 Int. Mask Reg port address */
#define IRQ4MSK     0xef      /* enable IRQ4 (COM1)          */
#define IRQ4OF 0x10           /* disable IRQ4                */
#define IVECT4  0x0c          /* comm. int vector for IRQ4   */
#define IRQ3MSK     0xf7      /* enable IRQ3 (COM2)          */
#define IRQ3OF  0x8      /* disable IRQ4                    */
#define IVECT3  0x0b          /* comm. int vector for IRQ3   */
#define RES8259 0x20          /* 8259 interrupt re_enable port */
#define RESINT  0x64          /* 8259 interrupt re_enable        */
#define SETIVEC 0x25          /* DOS system call to set int. vector   */
#define GETIVEC 0x35          /* DOS system call to get int. vector   */

                    /* CONNECT command parameters indicators*/
struct  parm{
     unsigned char pc_type;  /* specifies pc type                    */
     unsigned char port_no;  /* COM1 or COM2                         */
     unsigned char word_len; /* 8 or 7 bit word length               */
     unsigned char stop_bit; /* 2 or one stop bits                   */
     unsigned char parity;   /* even, odd, or no parity              */
     unsigned char speed;    /* 300,1200,2400,4800, or 9600 bps      */
     unsigned char modem;    /* Modem type e.g HAYES XXX or NULL     */
     unsigned char dial;     /* pulse or touch tone dialing          */
};

