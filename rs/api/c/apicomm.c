#include "GEV_DEFS.IN"
#include "D7TPS.IN"              /* for check_q() in disconnect  */
/**********************************************************************
*
*              **** EXTERNALS ****
*
***********************************************************************/
extern unsigned char *ip;         /* Current instruction pointer. */
extern Objstream stream_ptr;         /* Program stream pointer. */
extern struct oper optbl[];      /* Resolve operands table. */
extern unsigned int api_rtn_code;      /* Api return code. */
extern MCB *receive();               /* Receive message. */

extern unsigned int Sys_rtn_code;      /* System return code. */
extern unsigned int Sys_last_msg_id;   /* Global system variable. */

extern unsigned int Sys_tone_pulse;    /* 09:21:86 R.G.R. was sys_pulse_tone. */
/**********************************************************************
*
*              **** DEFINITIONS ****
*
***********************************************************************/
#define FM0_REQUEST 0x00
#define FM0_RESP    0x10
#define FM0_RESP_EXP     0x20

#define FM0LENGHT   0x10      /* length of DIA FM0 - 16 bytes */
#define SEND        1
#define ERROR       0

#define MSGIDMSK    0x01      /* mask for message id */
#define OPTHDRMSK   0x02      /* mask for optional header */
#define PRIORITYMSK 0x04      /* mask for priority */

#define NEW_TX_BLK  998       /* maximum size for a block */
#define MSB    1
#define LSB    0

/**********************************************************************
*
*              **** VARIABLE DECLARATIONS ****
*
***********************************************************************/
/* Union of integer and character array for fast hi byte access. */
union intary
{
unsigned char cary[2];          /* MSB and LSB of ivar. */
unsigned int ivar;         /* Integer variable. */
};
/********************************************************************
*
*           **** PROCESS CONNECT VERB ****
*
*********************************************************************
*
* FUNCTION:             enable_comm ()
*
* DESCRIPTION:
*      This module will issue a call to the communication manager
* to establish a connection with the series#1.
*
* INPUT:
*    .none
*
* OUTPUT:
*    .none
*
*
*     DATE           PROGRAMMER      REASON
* ---------------------------------------------------------------------
*   05/15/86        R.G.R.          ORIGINAL
*
***********************************************************************/
enable_comm ()
{

register struct oper *tele_num;

/* Initialize pointer to telephone number. */
tele_num = &optbl[0];

/* Return operand error if 1'st operand is a register or literal. */
#if dEBUG
if (tele_num->op_type <= ASC_TYPE)
{
    api_rtn_code = BUF_ERR;
    return;
}
#endif

#if !DEBUG_ALONE
/* Call Communication manager to perform line connect. */
     connect(tele_num->buffer,tele_num->buflen,Sys_tone_pulse);
#endif

}

/********************************************************************
*
*           **** PROCESS DISCONNECT VERB ****
*
*********************************************************************
*
* FUNCTION:             disable_comm ()
*
* DESCRIPTION:
*      This module will issue a call to the communication manager
* to disconnect the Reception device with the series#1.
*
* INPUT:
*    .none
*
* OUTPUT:
*    .none
*
*
*     DATE           PROGRAMMER      REASON
* ---------------------------------------------------------------------
*   05/15/86        R.G.R.          ORIGINAL
*
***********************************************************************/
disable_comm ()
{

#if !DEBUG_ALONE

/* Disconnect this line. */
disconnect ();
#endif


}

/********************************************************************
*
*       **** PROCESS SEND VERB (response required) ****
*
*********************************************************************
*
* FUNCTION:             tx_msg_yes ()
*
* DESCRIPTION:
*      This module will issue a call to the communication manager
* to transmit a message to the series#1.
*
* INPUT:
*    .none
*
* OUTPUT:
*    .none
*
*
*     DATE           PROGRAMMER      REASON
* ---------------------------------------------------------------------
*   05/15/86        R.G.R.          ORIGINAL
*   10/16/86        R.G.R.          VER. 4.0 Update
*   02/12/87        C. H.           Driver 7 support.
*                      Process SEND verb with message id.
*                      The message is a response if GEV
*                      'SYS_FM0_TXHDR' specify that the data
*                      mode is 'RESPONSE'. If this is the case,
*                      the msg_id is going to send to communi-
*                      cation manager. Otherwise, the msg_id will
*                      be discarded and receive a new message ID
*                      assigned by communication manager.
***********************************************************************/
tx_msg_yes ()
{
struct oper *msg_id;
GCB  sysgcb;
unsigned char req_id, send_ret ;

     /*Initialize pointer to message id.                         */
     msg_id = &optbl[1];

     /* Convert message id if not an integer register. */
     if (msg_id->op_type != INT_TYPE)
          req_id = asciibin(msg_id->buffer, msg_id->buflen);
     else
          req_id = *((unsigned int FAR *)msg_id->buffer);

     /* check up the data mode                         */
     /* message id is specified if the message is a response   */
     get_sys_gev(SYS_FM0_TXHDR,&sysgcb);
     if (  ( *(sysgcb.pointer+1) & FM0_RESP ) && req_id == 0 )
     {
          /* api_rtn_code = GEV_DIA_ERR ; */
           api_rtn_code = OPER_ERR ;
          return;
     }

     /* receive message id if this is a 'request'                     */
     if (  !( *(sysgcb.pointer+1) & FM0_RESP )   )
     {
          /* Release old message id.                     */
          RELBUF (msg_id);

          /* Get a buffer to receive message id.               */
          GETBUF (msg_id->entry->buffer,IALEN);

          /* Call to transmit message                  */
          msg_id->entry->buflen  =
          binascii ( tx_msg ( req_id ), msg_id->entry->buffer);
     }
     /*  response                                 */
     else
     {
        /* Call to transmit message                         */
        tx_msg( req_id );

        /* Release message id.                         */
        RELBUF (msg_id);
     }
}
/********************************************************************
*
*       **** PROCESS SEND VERB (no response required) ****
*
*********************************************************************
*
* FUNCTION:             tx_msg_no ()
*
* DESCRIPTION:
*      This module will issue a call to the communication manager
* to transmit a message to the series#1.
*
* INPUT:
*    .none
*
* OUTPUT:
*    .none
*
*
*     DATE           PROGRAMMER      REASON
* ---------------------------------------------------------------------
*   05/15/86        R.G.R.          ORIGINAL
*   02/12/87        C. H.           Driver 7 support.
*                      Process SEND verb without message id.
***********************************************************************/
tx_msg_no ()
{
unsigned char  messageid = 0 ;


/* Transmit message to comm line. */
tx_msg (messageid);

}
/********************************************************************
*
*           **** PROCESS SEND VERB  for Driver 7 ****
*
*********************************************************************
*
* FUNCTION:             tx_msg ()
*
* DESCRIPTION:
*      This module will issue a call to the communication manager
* to transmit a message to the series#1.
*
* INPUT:
*    .none
*
* OUTPUT:
*    .none
*
*
*     DATE           PROGRAMMER      REASON
* ---------------------------------------------------------------------
*   01/07/87        C. Hsieh     ORIGINAL
*   12/10/87        C. Hsieh     Bug Fix - too many acutal
*                           parameters.
*
***********************************************************************/
tx_msg (req_id)
unsigned char  req_id;
{
OPER        *message;    /* pointer to upstream of optbl entry */
union intary   time_out; /* TIMEOUT                    */
unsigned char  obitmap;  /* optional operand bit map        */
DIAPARM        *diainfoptr;   /* pointer to DIA info control blk */
DIAPARM        *b_diablk();

unsigned char  bpriority = 0; /* priority indicator */
unsigned char  hdrflag = 0 ;  /* optional hdr present indicator */
unsigned int   i;


/*  Get TIMEOUT(interval) (2 bytes) from instruction stream. */
time_out.cary[MSB] = *ip;

GETBYTE( ip, stream_ptr ) ;
time_out.cary[LSB] = *ip;

/*  Get OPT_HDR (1 byte) from instruction stream. */
GETBYTE( ip, stream_ptr ) ;
obitmap = *ip ;

/* Move ip ahead one byte so it points to next instruction. */
GETBYTE( ip, stream_ptr ) ;

/* initialize pointers to operand table entries */
message = &optbl[0];

/* optional header present or not */
if ( obitmap & OPTHDRMSK )
     hdrflag = 1 ;

/* priority set or not */
if ( obitmap & PRIORITYMSK )
     bpriority = 1 ;

/* create DIA control block */
if ( ( diainfoptr = b_diablk( hdrflag )) == NULL) /* 12/10/87 C.H. */
     return(ERROR);

#if !DEBUG_ALONE
/* Send message according to response flag. */
if ( diainfoptr->fm0_dmode & FM0_RESP_EXP    )
    /* Send message to comm line. */
    return( send(SEND, req_id, diainfoptr, message->buffer, message->buflen));
else
    send(SEND ,req_id, diainfoptr, message->buffer, message->buflen );

#endif
}
/********************************************************************
*
* FUNCTION:             b_diablk ()
*
* DESCRIPTION:
*      This module will create NEW DIA with system variable
*
* INPUT:
*
* OUTPUT:
*
*
*     DATE           PROGRAMMER      REASON
* ---------------------------------------------------------------------
*   01/07/87          C. Hsieh        ORIGINAL
*
***********************************************************************/
DIAPARM *b_diablk( hdrflag )
unsigned char  hdrflag;
{
DIAPARM   *diainfo;
union intary  fd_code;        /* holding area for fcode & datamode */
int       i;
GCB  sysgcb, sysgcb2;


/* get space for dia parameters blk */
if ( (diainfo = (DIAPARM *)malloc(sizeof(DIAPARM)) ) == NULL )
{
     api_rtn_code = BUF_ERR ;
     return((DIAPARM *)NULL );
}

/* set FM0 parameters by system variables */
get_sys_gev(SYS_FM0_TXHDR,&sysgcb);
diainfo->fm0_fcode = *(sysgcb.pointer);
diainfo->fm0_dmode = *(sysgcb.pointer+1);

/* if ( *(sysgcb.pointer+1) & FM0_RESP )
{
     api_rtn_code = GEV_DIA_ERR ;
     return((DIAPARM *) NULL);
}
*/

get_sys_gev(SYS_FM0_TXRID,&sysgcb);
if (  sysgcb.length == 0 )         /* default region ID      */
     diainfo->did[0] = 0x00;
else
     diainfo->did[0] = *(sysgcb.poiunter) ;

get_sys_gev(SYS_FM0_TXDID,&sysgcb);
/* if ( sysgcb.length == 0 )
{
     api_rtn_code = GEV_DIA_ERR ;
     return( (DIAPARM *) NULL);
}
else
{
*/
     for ( i=1; i < 4 ; i++)
     {
          diainfo->did[i] = *sysgcb.pointer ;
          ++sysgcb.pointer;
     }

/* check up the optional hdr */
if ( hdrflag )
{
   get_sys_gev(SYS_FM4_TXHDR,&sysgcb);
   get_sys_gev(SYS_FM64_TXHDR,&sysgcb2);

   if ( ( sysgcb.length == 0 ) && ( sysgcb2.length == 0 ))
       return((DIAPARM *) NULL );

   if ( sysgcb.length != 0 )       /* FM4 header exist */
   {
     if ((diainfo->fm4info = (FM4PARM *) malloc(sizeof(FM4PARM))) == NULL )
     {
          api_rtn_code = BUF_ERR ;
          return((DIAPARM *) NULL );
     }

     diainfo->fm4info->edmode = *(sysgcb.pointer);

     get_sys_gev(SYS_FM4_TXUSEID,&sysgcb);
     if (  sysgcb.length > 0 )
          xstrmove(diainfo->fm4info->ttxuid,sysgcb.pointer, 7 );
     else
          memset(diainfo->fm4info->ttxuid,'0',7);

     get_sys_gev(SYS_FM4_TXCORID,&sysgcb);
     if (  sysgcb.length > 0 )
        xstrmove(diainfo->fm4info->corrid, sysgcb.pointer, sysgcb.length);
     else
        diainfo->fm4info->corrid[0] = '\0' ;
     diainfo->fm4info->clen = sysgcb.length ;

   }
   else
     diainfo->fm4info = NULL;

   if ( sysgcb2.length != 0 ) /* FM64 header exist */
   {
     if ( (diainfo->fm64info = (FM64PARM *) malloc(sizeof(FM64PARM)))
          == NULL )
     {
          api_rtn_code = BUF_ERR;
          return((DIAPARM *) NULL );
     }

     diainfo->fm64info->status = *(sysgcb2.pointer) ;
     diainfo->fm64info->dmode = *(sysgcb2.pointer+1) ;

     /* get the length of FM64 text C.H. 8/10/87 */
     get_sys_gev(SYS_FM64_TXDATA,&sysgcb);

     /* FM64 header present */
     if ( sysgcb.length > 0 )
     {
        if (sysgcb.length > 512 )
        {
          api_rtn_code = OPER_ERR ;
          return;
        }

        diainfo->fm64info->txtlen = ( unsigned chort )sysgcb.length ;
        if( (diainfo->fm64info->text = malloc(sysgcb.length)) == NULL )
        {
          api_rtn_code = BUF_ERR ;
          return((DIAPARM *) NULL );
        }
        memcpy(diainfo->fm64info->text,sysgcb.pointer,sysgcb.length);
                      /*LB,LRZ 02/03/88  */
        /* multiple blk of FM64 messages are not allowed and  */
        /* this will be checked by build_dia() 8/10/87 C.H.  */
     }
     else
     {
       diainfo->fm64info->txtlen = 0 ;
       diainfo->fm64info->text = NULL ;
     }
   }
   else
     diainfo->fm64info = NULL ;
}
else
{
          diainfo->fm4info = NULL;
          diainfo->fm64info = NULL ;
}

diainfo->fm9info = NULL ;
/* return whole dia parameters blk to calling routine */

return(diainfo);
}


/********************************************************************
*
*          **** PROCESS RECEIVE VERB for Drive 7 ****
*
*********************************************************************
*
* FUNCTION:            rcv_msg ()
*
* DESCRIPTION:
*      This module will issue a call to the communication manager
* to receive a message from the communication line.
*
* INPUT:
*    .none
*
* OUTPUT:
*    .none
*
*
*     DATE           PROGRAMMER      REASON
* ---------------------------------------------------------------------
*   01/07/87         C. Hsieh        ORIGINAL
*
***********************************************************************/
rcv_msg ()
{
OPER      *msg_id;
RTE FAR   *message;
OPER      *msgoper;
OPER      *unsolicit;
DIAPARM   *rxdiaptr;
MCB       *mcbptr;
unsigned int   msgnum;
unsigned char  msgmode;

/* Initialize pointers. */
msg_id = &optbl[0];
msgoper = &optbl[1];

/* If necessary convert message id. */
if (msg_id->op_type == INT_TYPE)
    msgnum = *((unsigned int FAR *)msg_id->buffer);
else
    msgnum = asciibin (msg_id->buffer, msg_id->buflen);

#if !DEBUG_ALONE
/* Call the communication manager for receive data. */
if (  ( mcbptr = receive(msgnum)) == 0 )
{
     api_rtn_code = OPER_ERR ;     /* invalid msg id */
     return;
}

/* assign 'Sys_rtn_code'  and Check message status flag. */
if ( (Sys_rtn_code = mcbptr->msg_status) == OK )
{

    /* Move message to destination. */
    message = msgoper->entry;
    /* Free destination buffer for incoming message RDC 2/18/88 */
    if (message->buflen && message->buffer ) free(message->buffer ) ;

    message->buffer = mcbptr->rcv_msg;
    message->buflen = mcbptr->rcv_msg_len;

    /* set up GEV through DIA info return pointer */
    dia_to_sys( mcbptr->rcv_diaptr);

    /* release DIA info control blk  */
    free((char *)(mcbptr->rcv_diaptr));

}

#endif
}

/********************************************************************
*
* FUNCTION:            dia_to_sys ()
*
* DESCRIPTION:
*      This module restore dia information into GEVs.
*
* INPUT:
*    .dia information control block
*
* OUTPUT:
*
*
*     DATE           PROGRAMMER      REASON
* ---------------------------------------------------------------------
*   02/12/87         C. Hsieh        ORIGINAL
*
***********************************************************************/

dia_to_sys(diaptr)
DIAPARM *diaptr;
{
GCB  sysgcb ;

     sysgcb.pointer = &(diaptr->fm0_fcode );
     sysgcb.length = 2 ;
     put_sys_gev(SYS_FM0_RXHDR,&sysgcb);

     if ( diaptr->fm4info != NULL )
     {
          sysgcb.pointer = &(diaptr->fm4info->edmode) ;
          sysgcb.length = 1 ;
          put_sys_gev(SYS_FM4_RXHDR,&sysgcb);

          sysgcb.pointer = diaptr->fm4info->ttxuid ;
          sysgcb.length = 7 ;
          put_sys_gev(SYS_FM4_RXUSEID,&sysgcb);

          sysgcb.pointer = diaptr->fm4info->corrid ;
          sysgcb.length = diaptr->fm4info->clen ;
          put_sys_gev(SYS_FM4_RXCORID,&sysgcb);
          free( (char *) diaptr->fm4info );
     }

     if ( diaptr->fm64info != NULL )
     {
          sysgcb.pointer = &(diaptr->fm64info->status) ;
          sysgcb.length = 2 ;
          put_sys_gev(SYS_FM64_RXHDR,&sysgcb);

          /* 8/10/87     C.H. */
          if ( diaptr->fm64info->txtlen > 0 )
          {
               sysgcb.pointer = diaptr->fm64info->text ;
               sysgcb.length = diaptr->fm64info->txtlen ;
               put_sys_gev(SYS_FM64_RXDATA,&sysgcb);
               free( diaptr->fm64info->text );
          }

          free( (char *) diaptr->fm64info );
     }

     if (diaptr->fm9info != NULL )
         free( (char *) diaptr->fm9info );
}
/***************************************************************************
xstrmove() - string copy
****************************************************************************/
xstrmove(to , from, howmany)
unsigned char *to;
unsigned char *from;
unsigned short howmany;
{

     while( howmany-- )
          *to++ = *from++ ;
}

__relvuf(a)
struct oper *a;                    /* Ptr to operand table entry. */
{
   if ((a)->buflen != 0)
   {
      free((a)->buffer);
      (a)->buflen = 0;                  /* 12/10/87 CH */
      (a)->buffer = NULL;               /* 12/10/87 CH */
   }
}

__freebuf(a)
struct oper *a;                    /* Ptr to operand table entry. */
{
   if ((a)->cnv_type != NOTYP && (a)->buflen)
   {
      free((a)->buffer);
      (a)->buflen = 0;                  /* 12/10/87 CH */
      (a)->buffer = NULL;               /* 12/10/87 CH */
   }
}
