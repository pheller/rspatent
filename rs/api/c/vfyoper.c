/* API     = 1.1.2    LAST_UPDATE = 1.0.0    */
/********************************************************************
*
*                **** PROCESS API INSTRUCTION OPERANDS ****
*
*********************************************************************
*
* FILE NAME:                 VFYOPER.C
*
* DESCRIPTION:
*           This file contains the modules which will resolve the
* API instruction operands by updating entries within the operand
* table for ease of access by the instruction process modules.
*
*
* MODULES:
*              dprocoper () - process operands
*
*
*     DATE            PROGRAMMER          REASON
* --------------------------------------------------------------------
*   01/31/86            R.G.R.           ORIGINAL
*   03/20/86            A.J.M.           UPDATE
*   03/21/86            A.J.M.           UPDATE
*   03/26/86            R.G.R.           UPDATE
*   10/7/87             LA             Change PROTYPE to DEBUG option
*
**********************************************************************/
#include <SMDEF.IN>            /* Service manager definitions. */
#include <OBJUNIT.IN>          /* Object unit structure. */
#include <SMPPT.IN>            /* Service manager page template. */
#include <RTASTR.IN>           /* Run time array structure. */
#include <RTADEF.IN>           /* Run time array definitions. */
#include <INSERR.IN>           /* Instruction error return codes. */
#include <DBGMACRO.IN>          /* Packet handling macros. */
#include <stdio.h>


/*********************************************************************
*
*                    **** EXTERNALS ****
*
**********************************************************************/
extern unsigned char *ip;          /* Current instruction pointer. */
extern Objstream stream_ptr;       /* Program stream pointer. */
extern struct glbvlist * FAR glbmid[];/* Mid-ptr of global tree link list */
extern struct rta rtatbl[];        /* Run time array. */

/* LH changed w to W in following line, 2/3/87 */
extern WINDOW FAR *W;              /* Pointer to current window. */

/* Locate partition ext variable. */
extern struct pvarlist FAR *loc_extv();

/*********************************************************************
*
*                    **** DEFINITIONS ****
*
**********************************************************************/
#define MDOPER1      0x80           /* Mode mask for operand #1. */
#define RTLIMIT       255           /* Run time index limit. */
#define DATATYPE     0x7F           /* Indicates part, glob or RTA */
#define MSBRTA       0              /* MSB of run time array offset. */
#define BYTE3        0x80           /* 3 byte operand mask. */
#define EXTGLOBAL    0x03           /* Mask for indicating ext global */
#define EXTPARVAR    0x02           /* Mask for indicating ext par. */
#define RTAOFF       0x00           /* Mask for indicating RTA var. */
#define MSB          1              /* Used to get MSB of int var. */
#define LSB          0              /* Used to get LSB of int var. */
#define ENABLE       1              /* Enable ext par var search. */
#define DISABLE      0              /* Disable ext par var search. */
#define NOPARVAR     0              /* Used for checking locextv rtn */
#define NODATA       0              /* No data has been moved (nulls)*/
#define DATA         1              /* Data has been moved into buffer*/
#define RTA_ADJUST   64             /* RTA index adjustment. */
#define PAR_ADJUST   64             /* PAR index adjustment. */

/*********************************************************************
*
*                    **** VARIABLE DECLARATIONS ****
*
**********************************************************************/
struct oper optbl[9];
/*******************************************************************
*
*                **** PROCESS INSTRUCTION OPERAND S****
*
********************************************************************
*
* FUNCTION:   procoper(number of operands, flag to enable ext par search)
*
* DESCRIPTION:
*
*       This module is called by the individual instruction process
* modules so that the instruction operands can be resolved. The
* instruction operands are resolved by placing the operands buffer
* pointer, buffer length, entry pointer, and run time offset within
* the operand table.
*
*
* INPUT:
*        .number of operands within instruction.
*        .enable/disable ext par search.
*
* OUTPUT:
*       .can return with the following possible error conditions:
*
*              BAD_INX  - a decimal number was encountered as the
*                         3'rd byte index.
*
*              INX_OVER - 3'rd byte index + offset is greater than 255.
*
*              BAD_OFF  - Invalid run time offset encountered.
*
*              NO_PARV  - no partition variable found.
*
*              NO_GLBV  - no global variable found.
*
*     DATE            PROGRAMMER          REASON
* --------------------------------------------------------------------
*   02/05/86            R.G.R.           ORIGINAL
*
**********************************************************************/
dprocoper (nbr_opr,mode, parsrch,glbsrch)

unsigned int  nbr_opr;          /* Number of operands to process. */
unsigned int  mode;             /* Mode byte of complex opcode. */
unsigned int  parsrch;          /* Ext. par. var enable search flag. */
unsigned int  glbsrch;          /* Ext. glb var enable/disable search */

{
unsigned char mask;             /* Used to mask mode byte. */
unsigned int bytecnt;           /* Used in SETPTR */
union offset  rtindex;          /* 3'rd byte operand index. */
struct pvarlist FAR *listptr;   /* Ptr to ext link list entry. */
register struct oper *opentry;  /* Used for setting up operand table. */
register struct rta  *rtaptr;   /* pointer to run time array. */
unsigned char FAR *tbuf;        /* Temporary pointer. */
unsigned char cnt;              /* Loop count. */
unsigned int glb_name;          /* Global variable name. */
unsigned int par_name;          /* Partition variable name. */


/* Point to first operand. */
R_GETBYTE (ip,stream_ptr);

/* Initialize pointer to operand table. */
opentry = &optbl[0];

/* Setup to mask 1'st operand. */
mask = MDOPER1;

/* Loop until all operands are processed. */
for (; nbr_opr != 0 ; --nbr_opr)
{
    opentry->cnv_type = 0;

    /* Test for complex operand. */
    if ((mode & mask) != 0)
    {
       /* Place MSB of run time offset within operand table. */
       opentry -> rtoff.artoff[MSB] = *ip;

       /* Point to LSB of run time offset. */
       R_GETBYTE (ip,stream_ptr);

       /* Place LSB of run time offset within operand table. */
       opentry -> rtoff.artoff[LSB] = *ip;

       /* Initialize array offset. */
       rtindex.irtoff = 1;

       /* Test for a three byte complex operand (index). */
       if ((opentry ->rtoff.artoff[MSB] & BYTE3) != 0)
       {
          /* Strip MSB (bit) of MSB of run time offset within table. */
          opentry -> rtoff.artoff[MSB] &= DATATYPE;

          /* Point to index. */
          R_GETBYTE (ip,stream_ptr);

          /* Return error if index is a decimal or reserved register. */
          if (((*ip >= DEC1) && (*ip <= DEC2))
                             ||
                        (*ip < INT1))
          {
             /* Return bad index indication. */
             return (BAD_INX);
          }

          /* Get pointer to rTA index. */
          rtaptr = &rtatbl[*ip];

          /* If index is a inter register then don't convert
             simply initialize run time index.
          */
          if ((*ip >= INT1) && (*ip <= INT2))
          {
              /* Get integer number. */
              rtindex.irtoff = *((unsigned int FAR *)rtaptr->strptr);

          }else{
              /* Convert ascii field to binary integer. */
              rtindex.irtoff = asciibin (rtaptr->strptr, rtaptr->strlngth);
          }
          if (rtindex.irtoff == 0)
              rtindex.irtoff = 1;

          opentry->cnv_type = *ip;
       }

       /* If operand is a run time array offset then continue
          to update operand table fields.
       */
       if (opentry -> rtoff.irtoff <= RTA1)
       {
          /* Add index to run time offset within table. */
          opentry -> rtoff.irtoff +=  (rtindex.artoff[LSB] - 1);

          /* Get address of run time array entry. */
          (RTA *)opentry->entry = rtaptr = &rtatbl[opentry->rtoff.artoff[LSB]

          /* Update pointer, length, and type fields. */
          opentry -> buffer = rtaptr -> strptr;
          opentry -> buflen = rtaptr -> strlngth;

          if (opentry ->rtoff.irtoff > DEC2)
          {
              /* Perform adjustment to RTA offset. */
              if (opentry->rtoff.irtoff > RTA1)
                 opentry->rtoff.irtoff += RTA_ADJUST;

              opentry -> op_type = RTA_TYPE;
          }else{
              if (opentry->rtoff.irtoff > INT2)
                 opentry->op_type = DEC_TYPE;
              else
                 opentry->op_type = INT_TYPE;
          }

       }else{
           if ((opentry -> rtoff.irtoff >= RTA2)
                            &&
              (opentry -> rtoff.irtoff <= RTA3))
           {
              /* Add index to run time offset within table. */
              opentry -> rtoff.irtoff +=  (rtindex.artoff[LSB] - 1);

              /* Get address of run time array entry. */
              (RTA *)opentry->entry = rtaptr
                             = &rtatbl[((opentry->rtoff.irtoff - RTA2) + (RTA

              /* Update pointer, length, and type fields. */
              opentry -> buffer = rtaptr -> strptr;
              opentry -> buflen = rtaptr -> strlngth;
              opentry -> op_type = RTA_TYPE;

           }else{
              /* If operand is a external global variable and
                 if search is not disable then search for partition
                 entry.
              */
              if (opentry -> rtoff.irtoff >= GLB1)
              {
                  opentry -> op_type = GLB_TYPE;

                  /* Add index to run time offset within table. */
                  opentry -> rtoff.irtoff +=  (rtindex.irtoff - 1);

                  /* Compute global variable name. */
                  glb_name = opentry->rtoff.irtoff - GLB1;

                  if ((glbsrch & mask) != DISABLE)
                  {
                     /* Call to locate ext par variable and if a
                        return indicates variable not there then
                        return with error.
                     */
                     if ((listptr -
                         loc_extv (glb_name,glbmid))
                      == NOPARVAR)
                     {
                        return (NO_GLBV);
                     }else{
#if (!DEBUG)
                         /* Process global variable. */
                         global_proc (listptr);
#endif
                         /* Update remaining operand table entry fields. */
                         opentry -> buffer = listptr -> pvarbuf;
                         opentry -> buflen = listptr -> pvarlen;
                         opentry -> entry = (RTE *)listptr;
                     }
                  }
              }else{
                  /* If operand is a external partition variable and
                     if search is not disable then search for partition
                     entry.
                  */
                  if ((opentry->rtoff.irtoff >= PAR1)
                                &&
                     (opentry->rtoff.irtoff <= PAR2))
                  {
                      opentry->rtoff.irtoff += (rtindex.irtoff - 1);
                      if (opentry->rtoff.irtoff > PAR2)
                         opentry->rtoff.irtoff += PAR_ADJUST;
                  }else{
                      opentry->rtoff.irtoff += (rtindex.irtoff - 1);
                  }
                  if (((opentry -> rtoff.irtoff >= PAR1)
                                &&
                  (opentry -> rtoff.irtoff <= PAR2))
                                ||
                  ((opentry -> rtoff.irtoff >= PAR3)
                                &&
                  (opentry -> rtoff.irtoff <= PAR4)))
                  {
                      opentry -> op_type = PAR_TYPE;

                   /* Compute partition name. */
                   if (opentry->rtoff.irtoff <= PAR2)
                        par_name = (opentry->rtoff.irtoff - (PAR1 - 1));
                   else
                        par_name = (opentry->rtoff.irtoff - PAR2);

                      if (par_name <= 64)
                         opentry->rtoff.irtoff = par_name + 191;
                      else
                         opentry->rtoff.irtoff = par_name + 255;

                      if ((parsrch & mask) !=  DISABLE)
                      {
                         /* Call to locate ext par variable and if a
                            return indicates variable not there then
                            return with error.
                         */
                         if ((listptr =
                              loc_extv (par_name, &W->pvar_mid
                                        )) == NOPARVAR)
                         {
                            return (NO_PARV);
                         }else{
                             /* Update remainint operand table entry fields. */
                             opentry -> buffer = listptr -> pvarbuf;
                             opentry -> buflen = listptr -> pvarlen;
                             opentry -> entry = (RTE *)listptr;
                         }
                      }
                  }else{
                         /* Error bad run time offset. */
                         return (BAD_OFF);
                 }
             }
         }
    }
    /* Adjust instruction pointer to point to next operand. */
    R_GETBYTE (ip, stream_ptr);

}else{
     /* Operand is a simple operand (ie...RTA or literal.) */
     /* Show RTA offset within operand table entry. */
     opentry -> rtoff.artoff[MSB] = MSBRTA;

     /* Test for literal. */
     if ((opentry -> rtoff.artoff[LSB] = *ip) == 0)
     {
         opentry->op_type = LIST_TYPE;
         /* Operand is immediate data therefore update operand
            table fields.
         */
         /* Point to literal length. */
         R_GETBYTE (ip,stream_ptr);
         bytecnt = opentry -> buflen = *ip;

         /* Get buffer if length is not zero. */
         if (bytecnt != 0)
         {
             /* Get buffer of size literal length. */
             R_GETBUF (tbuf,bytecnt);

             /* Initialize pointer to literal string. */
             opentry -> buffer = tbuf;

             /* Initialize flag to show no data has been moved. */
             for (cnt=bytecnt;cnt != 0; --cnt)
             {
                 R_GETBYTE (ip,stream_ptr);
                 *(tbuf++) = *ip;
             }
         }

         /* Adjust instruction pointer to point to next operand. */
         R_GETBYTE (ip,stream_ptr);

     }else{
          if ((opentry -> rtoff.irtoff >= PAR1)
                           &&
             (opentry -> rtoff.irtoff <= PAR2))
          {
              opentry->op_type = PAR_TYPE;

              /* Compute partition name. */
              par_name = opentry->rtoff.irtoff - (PAR1 - 1);

              if ((parsrch & mask) !=  DISABLE)
              {
                 /* Call to locate ext par variable and if a
                    return indicates variable not there then
                    return with error.
                 */
                 if ((listptr =
                      loc_extv (par_name, &W->pvar_mid
                                )) == NOPARVAR)
                 {
                      return (NO_PARV);
                 }else{
                      /* Update remaining operand table entry fields. */
                      opentry -> buffer = listptr -> pvarbuf;
                      opentry -> buflen = listptr -> pvarlen;
                      opentry -> entry = (RTE *)listptr;
                 }
              }
          }else{
             /* Operand is a one byte run time array offset. */

             /* Get address of run time array entry. */
             (RTA *)opentry->entry = rtaptr = &rtatbl[opentry->rtoff.artoff

             /* Update the remaining operand fields for this entry. */
             opentry -> buffer = rtaptr -> strptr;
             opentry -> buflen = rtaptr -> strlngth;
             if (opentry->rtoff.irtoff > DEC2)
             {
                opentry->op_type = RTA_TYPE;
             }else{
               if (opentry->rtoff.irtoff > INT2)
               {
                  opentry->op_type = DEC_TYPE;
               }else{
                  opentry->op_type = INT_TYPE;
               }
             }
          }
          /* Adjust instruction pointer to point to next operand. */
          R_GETBYTE (ip,stream_ptr);
      }
  }
/* Point to next operand ttable entry. */
opentry++;

/* Shift to setup to mask next operand. */
mask >>= 1;

}

/* Return with no error indication. */
return (OK);

}

