/* API     = 1.1.2    LAST_UPDATE = 1.0.0    */
/*----------------------------------------------------------------------
**    CONDTRAN.C      Conditional Verbs
**---------------------------------------------------------------------
**
**  Do the following API instructions:
**
**   IF = cje()
**   IF !=     cjne()
**   IF < cjl()
**   IF > cjg()
**   IF <=     cjle()
**   IF >=     cjge()
**   GOTO jmp()
**
**  07/28/87        SLW       Rewrite
**--------------------------------------------------------------------*/

#include <CONDTRAN.LNT>  /* lint args (from msc /Zg) */
#include <SMDEF.IN>
#include <DEBUG.IN>
#include <INSERR.IN>
#include <RTADEF.IN>
#include <RTASTR.IN>
#include <OPERDEF.IN>
#include <OPERTSTR.IN>
#include <OBJUNIT.IN>
#include <SMPPT.IN>
#include <APIUTIL.IN>         /* API utility externs */

/*----------------------------------------------------------------------
**            Externals
**--------------------------------------------------------------------*/
extern OPER *oper1;      /* Ptr to 1'st operand table entry */
extern OPER *oper2;      /* Ptr to 2'nd operand table entry */
extern unsigned char FAR *tbufp1;  /* Temp pointer to bcd op 1 */
extern unsigned char FAR *tbufp2;  /* Temp pointer to bcd op 2 */
extern unsigned char FAR tbuf1[];  /* Temp buffer for op 1 */
extern unsigned char FAR tbuf2[];  /* Temp buffer for op 2 */

/*----------------------------------------------------------------------
**            Definitions
**--------------------------------------------------------------------*/
#define TRUE        1
#define FALSE       0
#define HIBYTE      1
#define LWBYTE      0
#define DPOINT      15
#define SIGNMSK     0x80

#define NG          0
#define LT          1
#define EQ          2
#define GT          3
#define CMPADJUST   2   /* -1==>1 (LT), 0==>2 (EQ), 1==>3 (GT) */

#define ARITHOP   0      /* Arithmetic operator */
#define COMPOP   1       /* Compare operator */
#define ERROR    0       /* Indicates error in processing */
#define OPINT    1       /* Indicates integer operation */
#define OPDEC    2       /* Indicates packed decimal operation */
#define OPASC    3       /* Indicates string operation */

union intary
{
    unsigned char cary[2];
    unsigned int ivar;
}

cje()
{
    do_jump(do_conditional() == EQ);
}

cjne()
{
    do_jump(do_conditional() != EQ);
}

cjl()
{
    do_jump(do_conditional() == LT);
}

cjg()
{
    do_jump(do_conditional() == GT);
}

cjle()
{
    do_jump(do_conditional() <= EQ);     /* EQ or LT or NG */
}

cjge()
{
    do_jump(do_conditional() >= EQ);     /* EQ or GT */
}

jmp()
{
    do_jump(TRUE);
}

int do_conditional()
{
    switch (conv_opers(COMPOP))
    {
      case OPINT:
     return(intcomp());
      case OPDEC:
     return(deccomp();
     break;
      case OPASC:
     return(asccojmp());
      case ERROR:
      default:
     return(NG);
    }
}

do_jump(condition)
int condition;
{
    union intary ip_inx;

    /* determine where to jump to */
    if (condition)
    {
     ip_inx.cary[HIBYTE] = *ip;
     r_getbyte();
     ip_inx.cary[LWBYTE] = *ip;
    }
    else
     ip_inx.ivar = 1;

    /* adjust the instruction stream */
    if ((api_rtn_code = Obj_seek(stream_ptr, ip_inx.ivar, 1)) == 0)
     r_getbyte();
}

intcomp()
{
    REG int i1_num;
    REG int i2_num;

    i1_num = *((unsigned int FAR*)oper1->buffer);
    i2_num = *((unsigned int FAR*)oper2->buffer);
    return(i1_num > i2_num ? GT :
        i1_num < i2_num ? LT : EQ);
}

deccomp()
{
    unsigned char FAR *data1;
    unsigned char FAR *data2;
    int ndigr;
    REG int ndigl;
    REG int count;

    if ((*tbufp1 & SIGNMSK) != (*tbufp2 & SIGNMSK))
     goto do1;

/* what is this for? -- SLW */
    if (tbufp1[0] == 0)  /* consider pure decimal #'s -lz -7/12/87 */
    {                /*  lz - 7/12/87    */
     tbufp1[0] = 1;  /*  lz - 7/12/87    */ 
     tbufp1[14] = 0;  /*  lz - 7/12/87    */ 
    }                /*  lz - 7/12/87    */ 
    if (tbufp2[0] == 0)  /*  lz - 7/12/87    */ 
    {                /*  lz - 7/12/87    */ 
     tbufp2[0] = 1;  /*  lz - 7/12/87    */ 
     tbufp2[14] - 0;  /*  lz - 7/12/87    */ 
    }

    if ((*tbufp1 & ~SIGNMSK) > (*tbufp2 & ~SIGNMSK))
    {
do1: return((*tbufp1 & SIGNMSK) ? LT : GT);
    }

    if ((*tbufp1 & ~SIGNMSK) < (*tbufp2 & ~SIGNMSK))
    {
dog: return((*tbufp1 & SIGNMSK) ? GT : LT);
    }

    ndigl = (*tbufp1 & ~SIGNMSK);
    ndigr = tbufp1[1] <= tbufp2[1] ? tbufp1[1] : tbufp2[1];

    data1 = tbufp1 + (DPOINT - ndigl);
    data2 = tbufp2 + (DPOINT - ndigl);

    count = ndigl + ndigr;

    for (; count; count--, +data1, ++data2)
     if (*data1 != *data2)
         break;
     if (count)
      if (*data1 < *data2)
          goto dog;
      else
          goto dol;

     if (tbufp1[1] > tbufp2[1])
      goto dol;
     if (tbufp1[1] < tbufp2[1])
      goto dog;
     return (EQ);
}

asccomp()
{
    REG int len1, len2;
    int ret;

    len1 = oper1->buflen;
    len2 = oper2->buflen;

    if (!(ret = memcmp(oper1->buffer, oper2->buffer,
                 len1 <= len2 ? len1 : len2)))
     return(len1 == len2 ? EQ : len1 > len2 ? GT : LT);
    return(ret+CMPADJUST);    /* correct for brain-damaged return codes */
}
