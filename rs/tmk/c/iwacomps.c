/* VERSION = 6.1.2    LAST_UPDATE = 5.9.0    */
/********************************************************************/
/*                                          */
/* IWACOMPS - Complete an item from the request queue           */
/*                                          */
/*    DESCRIPTION - this subroutine provides an interface to the */
/*       multitasker SVC that formally completes a request.     */
/*                                          */
/*    HOW INVOKED - via call of vstcomp with one parameter      */
/*               compreq(tag)                    */
/*                                          */
/*    INPUTS -                              */
/*        PARAMETERS:                            */
/*          tag:  request que element identifier as returned     */
/*             by the SVC handler in response to an earlier   */
/*             "get request" call                             */
/*                                          */
/*    SUBROUTINES -                              */
/*        EXTERNAL:                              */
/*        issuesvc: executes the svc                  */
/*                                          */
/********************************************************************/
#include <iwssvcsc.h>                    /* svc structure maps      */
#include <iwssvccc.h>                    /* svc constants           */

cmopreq(tag)
int tag;
{
 extern issuesvc();
 struct svcregs svcregs;
  svcregs.sr_indx = 0;             /* init ax to 0         */
  svcregs.sr_type = _QSVCCMPRQ;    /* complete request code    */
  svcregs.sr_spec = tag;      /* que item number      */
#ifndef M_I86LM
  issuesvc(0,&svcregs);
#else
  issuesvc((long)0,&svcregs);
#endif
}
