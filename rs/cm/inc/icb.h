/* CM      = 1.1.2    LAST_UPDATE = 1.0.0    */
/*
 *
 *    SS 09/24/87   Added NDCNCTD status to tell SM that physical
 *             task is unable to hangup the modem and LINERR
 *             for modem result codes 6 to 9 since they may
 *             not be compatible. Also replaced PORTDN with
 *             MTMOUT for modem time out.
*/
/************************************************************************/
/* icb.h - Intertask Communication Structures.               */

                         /* delivery of block to SM_T  */
struct lkdbk{
          struct pkt *nxpk;   /* data packet address        */
          unsigned char pklen;     /* data length from link header */
          unsigned char byte; /* 1st byte of data after header*/
};

                         /* parameter block from SM_T  */
struct smps{
          char *msg;          /* message address       */
          unsigned msgl;      /* message length        */
          unsigned long dest; /* destination           */
          unsigned char pri;  /* priority              */
          unsigned char diamd;     /* dia request/response   */
          unsigned long msgid;     /* message id            */
};    

                         /* block delivery LTM_T->TX_T */
struct lkblk{
          char *start;        /* beginning of block         */
          short len;          /* length of block       */
          short cnt;          /* count for packetizing */
          struct pkt *pks[4]; /* address of packets         */
          unsigned short lastpk;   /* last pkt within block (0-3) */
          unsigned short xfrd;     /* count of transferred packets */
          struct lkblk *nxt;  /* next block structure       */
};

                         /* intertask message structure     */
struct icb {
          unsigned char sender;    /* sender task           */
          unsigned char type; /* message type               */
          unsigned char function;  /* function for receiving task */
          unsigned char status;    /* return status         */
          union {
              struct pkt *pktp;    /* packet link header pointer   */
              struct dias *blkp;   /* DIA header pointer     */
              char *fonep;    /* phone # pointer when CONNECT */
              struct smps smp;     /* parameters sent by SM to LTMT*/
              struct lkblk *ltbk; /*used when LTMT forwards 1k
                           blocks TXT transmit            */
              struct lkdbk *rxpk; /* used when LTMT forwards packets
                           to SM                          */
          }u;
};


                         /* task ids                      */
#define   LTM_T     6              /* local transport manager */
#define RX_T   7              /* receive link               */
#define   TX_T 8              /* transmit link         */
#define   PHY_T     9              /* physical functions     */
#define   IH_T 10             /* device handler        */
#define SM_T   11             /* service manager       */

                         /* Intertask queueing priority     */
#define   BOTTOM    0              /* place at end of rcve queue */
#define TOP    1              /* place at begng of rcve queue */

                         /* intertask communication type    */
#define   ASYRQ     1              /* asynchronous request   */
#define ASYRS  2              /* asynchronous response */
#define SYRQ   3              /* synchronous request        */
#define   SYRS 4              /* synchronous response       */

                         /* ICB functions         */
#define   CLAIML    0              /* request for line use   */
#define   RELSL     1              /* return of line for pkt mode */
#define ERROR   2             /* any error             */
#define ABORT  3              /* abort            */
#define SEND   4              /* send data             */
#define RCVE   5              /* receive data               */
#define CNECT  6              /* connect command       */
#define   DCNECT    7              /* disconnect command     */
#define   CWAIT     8              /* call wait             */
#define   TEST      9              /* perform test command   */
#define   CONX25    10             /* connect for x.25 mode */
#define   DCONX25   11             /* disconnect for x.25 mode */
#define   WAKE 12             /* intertask resume msg  */

                         /* ICB status            */
#define NOERR  0              /* function performed ok */
#define MEMER  1              /* memory allocation error    */
#define PKTER  2              /* packet memory error        */

                         /* ICB status (PHY_T to SM_T) */
#define   CONCTD    0              /* line connected        */
#define   DCNCTD    1              /* line disconnected      */
#define   NDCNCTD   2              /* can't disconnect line */
#define NOCAR  3              /* data carrier loss          */
#define NODTN  4              /* no dial tone               */
#define SMBUSY 5              /* line busy             */
#define NOANSR 6              /* no answer             */
#define MTMOUT 7              /* modem time out        */
#define LINERR  8             /* modem error codes 6 to 9   */
#define IRXQSZ      20             /* size of RX_T i queue   */

                         /* VOCABULARY            */
#define   GOOD 0
#define   BAD  1
#define   NO   0
#define   YES  1
#define   EMPTY     0
#define   FULL 1
#define   OK   1
#define   ON   1
#define   OFF  0
#define NOLINE 3    /* cannot connect  (LH, 3/26/87) */
