/* LOS     = 1.0.0    LAST_UPDATE = 1.0.0    */
#include <iwssvcsc.h>
#include <fcntl.h>
#include <sys\types.h>
#include <sys\stat.h>
#include <io.h>

char far *loader(string, environ)
char *string;
struct env *environ;
{

int open();
int read();
unsigned long getm();
void copy_header();

struct PROGRAM_HEADER {
     unsigned int   signature;
     unsigned int   length_remainder;
     unsigned int   length_pages;      /* 512 byte segments */
     unsigned int   relocation_count;
     unsigned int   header_paragraphs;
     unsigned int   memory_min;
     unsigned int   memory_max;
     unsigned int   ss_offset;
     unsigned int   sp_init;
     unsigned int   checksum;
     unsigned int   ip_init;
     unsigned int   code_offset;
     unsigned int   first_relocation;
     unsigned int   overlay;
} exe_header;
#define   HEADER_SIZE    14*2;
#define   HEADER_ID 0x4A5D

struct LONG_ADDRESS {
     union {
          struct{
          unsigned int offset;
          unsigned int segment;
          } word;
          unsigned int *pointer;
          char far *base;
          unsigned long big;
     } addr;
};
struct LONG_ADDRESS program_segment;
struct LONG_ADDRESS relocation_address;
struct LONG_ADDRESS section_read;

int       file_id;
unsigned int   amount_read;
unsigned long  file_size;
unsigned long  memory_size;
unsigned int   base_paragraph;
unsigned int   max_paragraph;
unsigned long  code_start;

/* get the file specified and loader in the program header   */
     file_id=open( string, O_RDONLY | O_BINARY );
     if (file_id < 0) {
          perror ("TMK/LOS Loader:  Cannot find file\n");
          exit();
     }
     read(file_id, &exe_header, HEADER_SIZE);

/* calculate file size for memory allocation                */
     file_size =
          ((unsigned long)(exe_header.length_pages - 1) * 512) +
          (unsigned long)(exe_header.length_remainder);

/* the code start is after the relocation table              */
     code_start =
          (unsigned long)(exe_header.header_paragraphs * 0x10);

/* calculate the size of the executable portion of the file  */
     file_size -= code_start;

/* allocate memory for this beast and check for error code   */
/* remember that we need executable, data space, psp, and environment */
     memory_size = file_size +
               (unsigned long)(exe_header.memory_min * 0x10) +
               0x100 + 0x400;

/* make this paragraph bound                           */
     memory_size -= (memory_size % 0x10);
     program_segment.addr.big = getm(memory_size);

     if (program_segment.addr.word.segment == 0x8){
          puts("TMK/LOS Loader: Not enough memory to load\n");
          exit();
     }

/* paragraph align the segment executable address            */
/* --- the DOS memory manager already segment aligns allocated memory */
/*   program_segment.addr.word.offset += 0x100;*/
/*   program_segment.addr.word.segment +=
               (program_segment.addr.word.offset / 0x10);*/
/*   program_segment.addr.word.offset = 0;*/

/* add space for the psp                          */
     program_segment.addr.word.segment += ( 0x100 / 0x10 );

/* calculate paragraph start or stop                        */
     base_paragraph = program_segment.addr.word.segment - (0x100/0x10);
     max_paragraph = base_paragraph + (int)(memory_size / 0x10);
/*
 *   The new program segment is 100h bytes lower than the program
 *   starts so let copy the program segment header and
 *   update it for our new program.  Values passed must be in
 *   segment format.
 */
/* pass the psp base address and the maximum memory paragraph  */
     copy_header(base_paragraph, max_paragraph);

/* seek to program portion of file and load it               */
/* files may be >32767 bytes, so read the file in chunks     */
     section_read.addr.base = program_segment.addr.base;
     lseek(file_id, code_start, 0);
     while ( file_size > 0x7FF0 ){
          amount_read =
               read(file_id, section_read.addr.base,
                    0x7FF0);
     if ( amount_read != 0x7FF0 ) {
          puts("TMK/LOS Loader:  incorrect code size in PSP\n");
          exit();
     }

/* adjust program load point                           */
/* we must play games because MSC doesn't support address math  */
/* so, adding 0x7FF to the segment is = adding 0x7FF0 to the offset */
          section_read.addr.word.segment += 0x7FF;

/* adjust number of bytes needed to read                    */
          file_size -=
               (unsigned long)amount_read;
     }

/* read remainder, if any                              */
     if ( file_size )
          amount_read = read(file_id, section_read.addr.base.
               (unsigned int)file_size);

/* look through the relocation table and make the changes    */
     lseek(file_id, (long) exe_header.first_relocation, 0);

     do {
          read(file_id, &relocation_address, 4);

          relocation_address.addr.word.segment +=
               program_segment.addr.word.segment;

          *relocation_address.addr.pointer +=
               program_segment.addr.word.segment;

     }  while (--exe_header.relocation_count);

     close(file_id);

     printf("Symbol Table Program Segment Offset: %4x\n",
          program_segment.addr.word.segment);

     printf("Instruction Pointer: = %4x\n\n", exe_header.ip_init);
     /* point es and ds to __psp for benefit of c startup */
     environ -> en_ds = program_segment.addr.word.segment - 0x10;
     environ -> en_es = program_segment.addr.word.segment - 0x10;
     program_segment.addr.word.segment += exe_header.code_offset;
     program_segment.addr.word.offset = exe_header.ip_init;
     return (program_segment.addr.base);
}
