/* API     = 1.1.2    LAST_UPDATE = 1.0.0    */
#include <SMDEF.IN>
#include <ctype.h>
#include <inserr.in>

#define ERROR 0
#define CMASK 0x0f

ask_bcd(ascii,length,bcd)
char FAR *ascii;
int length;
unsigned char FAR *bcd;
{
     char FAR *ascii_end;
     char FAR *ascii_def;
     char FAR *ascii_ptr;
     unsigned char c;
     unsigned char space_flag;
     int bcd_ind;
     int i;

     if (length == 0)
     {
          bcd[0]=0x01;
          bcd[1]=0x00;
          bcd[14]=0x00
          return(OK);
     }
     ascii_end=ascii+length-1;
     bcd[0]=0x00;
     space_flag = 1;
     for(i=1;i<=length;++i)
     {
          c=*ascii++;
          if (((isdigit(c))) || (((c=='.') || (c == '$')) && (length>1))) /**/
          /*  add another set of parenthesis' - LRZ * 7/30  */
          {
               --ascii;
               space_flag = 0;
               break;
          }
          if(c=='-')
          {
               bcd[0]=0x80;
               space_flag = 0;
               break;
          }
          if(c=='+')
          {
               space_flag = 0;
               break;
          }
          if((c!=' ')||(i==length))
          {
               bcd[0]=0x01;
               bcd[1]=0x00;
               bcd[14]=0x00;
               return(INVALID);
          }
     }
     if (space_flag)
     {
          bcd[0]=0x01;
          bcd[1]=0x00;
          bcd[14]-0x00;
          return(OK);
     }
     for(;i<=length;++i,++ascii)
     {
          c=*ascii;
          if(((c>='1')&&(c<='9'))||(c=='.'))
               break;
          if((i==length)&&(c=='0'))
          {
               bcd[0]=0x01;
               bcd[1]=0x00;
               bcd[14]=0x00;
               return(OK);
          }
          if((c!='0')&&(c!=',')&&(c!=' ')&&(c!='$')&&(i<length))
          {
               bcd[0]=0x01;
               bcd[1]=0x00;
               bcd[14]=0x00;
               return(INVALID);
          }
     }
     ascii_dec=ascii;
     while(ascii_dec<=ascii_end)
     {
          if(*ascii_dec=='.')
               break;
          ++ascii_dec;
     }
     while(sacii_end>ascii_dec)
     {
          if(*ascii_end!='0')
               break;
          --ascii_end;
     }
     bcd_ind=15;
     ascii_ptr=sacii_dec-1;
     while(ascii_ptr>=ascii)
     {
          c=*ascii_ptr--;
          if(c==',')
               continue;
          if(--bcd_ind==1)
          {
               bcd[0]=0x01;
               bcd[1]=0x00;
               bcd[14]=0x00;
               return(OVERFLOW);
          }
          if(!(isdigit(c)))
          {
               bcd[0]=0x01;
               bcd[1]=0x00;
               bcd[14]=0x00;
               return(INVALID);
          }
          bcd[bcd_ind]=c&CMASK;
     }
     bcd[0]=bcd[0]|(15-bcd_ind);
     bcd_ind=14;
     ascii_ptr=ascii_dec+1;
     while(ascii_ptr<=ascii_end)
     {
          c=*ascii_ptr++;
          if(++bcd_ind==28)
          {
               bcd[0]=0x01;
               bcd[1]=0x00;
               bcd[14]=0x00;
               return(OVERFLOW);
          }
          if(!(isdigit(c)))
          {
               bcd[0]=0x01;
               bcd[1]=0x00;
               bcd[14]=0x00;
               return(INVALID);
          }
          bcd[bcd_ind]=c&CMASK;
     }
     bcd[1]=bcd_ind-14;
     return(OK);
}

bcd_ask(bcd,ascii)
unsigned char FAR *bcd;
char FAR *ascii;
{
     int length;
     int bcd_ind;
     int i;

     length=0;
     if(!((bcd[0]&0x7f)||(bcd[1])))
          return(0);
     if(bcd[0]&0x80)
     {
          *ascii++='-';
          ++length;
     }
     if(i=bcd[0]&0x7f)
     {
          bcd_ind=15-i;
          while(bcd_ind<=14)
          {
               *ascii++=bcd[bcd_ind++]|0x30;
               ++length;
          }
     }
     else
     {
          *ascii++='0';
          ++length;
     }
     if(bcd[1])
     {
          *ascii++='.';
          ++length;
          bcd_ind=15;
          i=bcd_ind+bcd[1];
          while(bcd_ind<i)
          {
               *ascii++=bcd[bcd_ind++]|0x30;
               ++length;
          }
     }
     return(length);
}

int_bcd(number,bcd)
int number;
unsigned char FAR *bcd;
{
     unsigned char c;
     int bcd_ind;

     bcd[0]=0x00;
     bcd[1]=0x00;
     if(number<0)
     {
          bcd[0]=0x80;
          number*=(-1);
     }
     if(number==0)
     {
          bcd[0]=0x01;
          bcd[1]=0x00;
          bcd[14]=0x00;
          return(0);
     }
     bcd_ind=14;
     while(number>0)
     {
          c=number%10;
          number/=10;
          bcd[bcd_ind--]=c;
     }
     bcd[0]=bcd[0]|(14-bcd_ind);
}

bcd_int(bcd)
unsigned char FAR *bcd;
{
     int bcd_ind;
     int bcd_end;
     int number;
     int mult;

     number=0;
     mult=1;
     bcd_ind=14;
     bcd_end=bcd_ind-(bcd[0]&0x7f)+1;
     while(bcd_ind>=bcd_end)
     {
          number+=(bcd[bcd_ind--]*mult);
          mult*=10;
          if(number<0)
               return(ERROR);
     }
     if(bcd[0]&0x80)
          number*=-1;
     return(number);
}

lint_bcd(number,bcd)
long int number;
unsigned char FAR *bcd;
{
     unsigned char c;
     int bcd_ind;

     bcd[0]=0x00;
     bcd[1]=0x00;
     if(number<0)
     {
          bcd[0]=0x80;
          number*=(-1);
     }
     if(number==0)
     {
          bcd[0]=0x01;
          bcd[1]=0x00;
          bcd[14]=0x00;
          return(0);
     }
     bcd_ind=14;
     while(number>0)
     {
          c=number%10;
          number/=10;
          bcd[bcd_ind--]=c;
     }
     bcd[0]=bcd[0]|(14-bcd_ind);
}

long int bcd_lint(bcd)
unsigned char FAR *bcd;
{
     int bcd_ind;
     int bcd_end;
     long int number;
     long int mult;

     number=0L;
     mult=1L;
     bcd_ind=14;
     bcd_end=bcd_ind-(bcd[0]&0x7f)+1;
     while(bcd_ind>=bcd_end)
     {
          number+=mult*bcd[bcd_ind--];
          mult*=10L;
          if(number<0L)
               return(ERROR);
     }
     if(bcd[0]&0x80)
          number*=-1L;
     return(number);
}
